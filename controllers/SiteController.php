<?php

namespace app\controllers;

use app\commands\TranslateController;
use app\models\AddServices;
use app\models\CityList;
use app\models\Continent;
use app\models\CountryList;
use app\models\DayForTur;
use app\models\RegForm;
use app\models\ReviewsTour;
use app\models\Tur;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\Pagination;
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
      return [
              'error' => [
                'class' => 'yii\web\ErrorAction',
              ],
              'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
              ],
            ];
    }

    public function actionIndex()
    {
        $hot = Tur::find()->where('sale >= 1')->orderBy(['sale'=>SORT_DESC])->limit(6)->all();
        $place = '';
        foreach($hot as $h){
            if(!empty($c = $h->city->title)){
                if(!preg_match("/".$c."/i",$place)){
                    $place.=$c.', ';
                }
            }else{
                $c = $h->country->title;
                if(!preg_match("/".$c."/i",$place)){
                    $place.=$c.', ';
                }
            }
        }


        $place=trim($place, ", ");
        //echo "<pre>".print_r($place,true)."</pre>";die();
        $recomends = Tur::find()->where('visit>0')->orderBy(['visit'=>SORT_DESC])->limit(6)->all();
        $continent = Continent::find()->all();
        $grup=[];
        foreach($continent as $rec){
                $grup[str_replace(' ','',$rec->name)]=[];
        }
        foreach($recomends as $rec){
            if(!empty($rec->continent->name)){
                $grup[$rec->continent->name][]=$rec;
            }
        }


        $reviews_tour = ReviewsTour::find()->orderBy(['update_at'=>SORT_DESC])->limit(4)->all();
        //echo "<pre>".print_r($grup,true)."</pre>";die();
        return $this->render('index',[
            'hot'=>$hot,
            'place' => $place,
            'recomends'=>$recomends,
            'grup'=>$grup,
            'continent'=>$continent,
            'reviews_tour'=> $reviews_tour
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContacts()
    {
        return $this->render('contacts');
    }
    public function actionPayment()
    {
        return $this->render('payment');
    }

    public function actionAbout()
    {
        return $this->render('about');
    }


    public function actionReg(){
        $model = new RegForm();
        //echo "<pre>".print_r($model,true)."<pre>";die();
        if( $model->load(Yii::$app->request->post()) && $model->validate()){
            if($user  = $model->reg()){
                //echo "<pre>".print_r($user,true)."<pre>";die();
                if($user->status === User::STATUS_ACTIVE){
                    if(Yii::$app->getUser()->login($user)){
                        return $this->goHome();
                    }
                    else{
                        Yii::$app->session->setFlash('error',TranslateController::t('There was an error when registering'));
                        Yii::error(TranslateController::t('There was an error when registering'));
                        return $this->refresh();
                    }
                }
            }
        }
        return $this->render('reg',[
            'model' =>$model
        ]);
    }


    public function actionFindCity(){
        $id = $_POST['id'];
        $city = CityList::find()->where('country_id=:id',[':id'=>$id])->asArray()->all();
        return json_encode($city);
    }

    public function actionTourList()
    {
        $query = Tur::find();
        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination(['totalCount' => $count,'defaultPageSize'=>20]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $country = ArrayHelper::map(CountryList::find()->all(),'id','title');
        return $this->render('tour-list', [
            'models' => $models,
            'pages' => $pages,
            'count'=>$count,
            'country'=>$country
        ]);
    }

    public function actionTourDetail($id){
        $model =Tur::findOne($id);
        $day_tur = DayForTur::find()->where(['tur_id'=>$id])->orderBy(['number_day'=>SORT_ASC])->all();
        $add_servises = AddServices::find()->where(['tur_id'=>$id])->all();

        $responses=[];
        foreach ($add_servises as $add_servise) {
            $place =$add_servise->place->country->title.' - '.$add_servise->place->place.', '.$add_servise->place->city->state;
            $responses[$place][$add_servise->name]['description']=$add_servise->description;
            $responses[$place][$add_servise->name]['price']=$add_servise->price;
            $responses[$place][$add_servise->name]['price_type']=$add_servise->price_type;
        }

       // echo "<pre>".print_r($response,true)."<pre>";die();

        return $this->render('tour-detail',[
            'model'=>$model,
            'day_tur'=>$day_tur,
            'responses'=>$responses,
        ]);
    }


    public function actionTurFilter(){
        //echo "<pre>".print_r($_POST,true)."<pre>";die();
        $tmp=[];
        if(!empty($start=explode(',',$_POST['region']))){
            //echo "<pre>".print_r(explode(',',$_POST['region']),true)."<pre>";die();
            if(!empty($start[1])&&!empty($start[1])){
                $cyties = CityList::find()
                    ->andWhere(['like', 'title',$start[1]])
                    ->andWhere(['like', 'region',$start[0]])->all();;

                foreach ($cyties as $country) {
                    $tmp['city'][]= $country->id;
                }
            }
        }
        $models = Tur::find();
        if(!empty($start=strtotime($_POST['start']))){
            $models=$models->andWhere(['start'=>$start]);
        }
        if(!empty($start=explode(',',$_POST['price_range']))){
            $models=$models->andWhere('prise>=:stm',[':stm'=>$start[0]]);
        }
        if(!empty($start[1])){
            $models=$models->andWhere('prise<=:stt',[':stt'=>$start[1]]);
        }
        if(!empty($country=$_POST['guest'])){
            $models=$models->andWhere('country_id=:country_id',[':country_id'=>$country]);
        }if(!empty($country=$_POST['Check'])){
            $tm=[];
            foreach ($country as $key=>$value) {
                $tm[]=$key;
            }

            $models=$models->andWhere('country_id=:country_id_list',[':country_id_list'=>$tm]);
        }
        if(!empty($tmp['city'])){
            $models=$models->andWhere(['city_id'=>$tmp['city']]);
        }
        if(!empty($end=strtotime($_POST['end']))){
            $models=$models->andWhere(['end'=>$end]);
        }
        if(!empty($length=$_POST['length'])){
            if(is_numeric($length)){
                $models=$models->andWhere(['long_time'=>$length]);
            }
            else{
                $models=$models->andWhere('long_time>=:long_time',[':long_time'=>4]);
            }

        }
        if(!empty($price=$_POST['price'])){
            $models=$models->orderBy(['prise'=>$price]);
        }

        $models = $models->all();

        return $this->renderPartial('_tour-search',[
            'models' => $models
        ]);
    }


    public function actionTurSearch(){
        $response=[];
        if(isset($_POST['form']['region']) &&!empty($region=$_POST['form']['region'])){
            $region = json_encode( explode($region,','));

            echo "<pre>".print_r($region,true)."<pre>";
        }
        elseif(isset($_POST['guest'])){

        }
    }

    public function actionFlightList(){
        return $this->render('flight-list');
    }

    public function actionCart(){
        return $this->render('cart');
    }
    public function actionPackageList(){
        return $this->render('package-list');
    }
    public function actionHomeHotel(){
        return $this->render('home-hotel');
    }

}
