<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;

/**
 * This is the model class for table "hotel_residents".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $hotel_id
 * @property integer $room_type_id
 * @property integer $check_in
 * @property integer $check_out
 *
 * @property Hotel $hotel
 * @property RoomType $roomType
 * @property User $user
 */
class HotelResidents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_residents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'hotel_id', 'room_type_id', 'check_in', 'check_out'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => TranslateController::t('User'),
            'hotel_id' => TranslateController::t('Hotel'),
            'room_type_id' => TranslateController::t('Room Type'),
            'check_in' => TranslateController::t('Check In'),
            'check_out' => TranslateController::t('Check Out'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomType()
    {
        return $this->hasOne(RoomType::className(), ['id' => 'room_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
