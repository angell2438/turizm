<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;

/**
 * This is the model class for table "crm_country_list".
 *
 * @property integer $id
 * @property string $title
 * @property string $iso_code
 * @property integer $status
 *
 * @property CityList[] $crmCityLists
 * @property DayForTur[] $dayForTurs
 * @property DayForTur[] $dayForTurs0
 * @property Hotel[] $hotels
 * @property PlaceAddServices[] $placeAddServices
 * @property Tur[] $turs
 */
class CountryList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crm_country_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status'], 'integer'],
            [['title', 'iso_code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' =>TranslateController::t('Title'),
            'iso_code' => TranslateController::t('Iso Code'),
            'status' =>TranslateController::t( 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrmCityLists()
    {
        return $this->hasMany(CityList::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDayForTurs()
    {
        return $this->hasMany(DayForTur::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDayForTurs0()
    {
        return $this->hasMany(DayForTur::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotels()
    {
        return $this->hasMany(Hotel::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceAddServices()
    {
        return $this->hasMany(PlaceAddServices::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurs()
    {
        return $this->hasMany(Tur::className(), ['country_id' => 'id']);
    }
}
