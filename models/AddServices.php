<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;

/**
 * This is the model class for table "add_services".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $price
 * @property string $price_type
 * @property integer $tur_id
 * @property integer $place_id
 *
 * @property PlaceAddServices $place
 * @property Tur $tur
 */
class AddServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'add_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','price', 'tur_id', 'place_id'], 'required'],
            [['name'],'unique'],
            [['price', 'tur_id', 'place_id'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['price_type'], 'string', 'max' => 55]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => TranslateController::t('Name'),
            'description' =>TranslateController::t('Description'),
            'price' =>TranslateController::t('Price'),
            'price_type' =>TranslateController::t('Price for'),
            'tur_id' =>TranslateController::t('Tur name'),
            'place_id' =>TranslateController::t('Place'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(PlaceAddServices::className(), ['id' => 'place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTur()
    {
        return $this->hasOne(Tur::className(), ['id' => 'tur_id']);
    }
}
