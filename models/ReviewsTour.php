<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "reviews_tour".
 *
 * @property integer $id
 * @property integer $tur_id
 * @property integer $user_id
 * @property string $transport_type
 * @property integer $create_at
 * @property integer $update_at
 * @property string $description
 *
 * @property Tur $tur
 * @property User $user
 */
class ReviewsTour extends ActiveRecord
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }


    public static function tableName()
    {
        return 'reviews_tour';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tur_id', 'user_id', 'create_at', 'update_at','type_review'], 'integer'],
            [['description','name'], 'string'],
            [['transport_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tur_id' => TranslateController::t('Tour'),
            'user_id' => TranslateController::t('User'),
            'transport_type' => TranslateController::t('Transport Type'),
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'description' => TranslateController::t('Description'),
            'name' => TranslateController::t('Name'),
            'type_review' => TranslateController::t('Type review'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTur()
    {
        return $this->hasOne(Tur::className(), ['id' => 'tur_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
