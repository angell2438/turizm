<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;

/**
 * This is the model class for table "room_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $prise
 * @property integer $count
 * @property integer $free
 * @property integer $hotel_id
 * @property integer $number_seats
 * @property integer $count_room
 *
 * @property HotelResidents[] $hotelResidents
 * @property Hotel $hotel
 */
class RoomType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'prise'], 'required'],
            [['prise', 'count', 'free', 'hotel_id', 'number_seats', 'count_room'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['img', 'description'],'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => TranslateController::t('Name'),
            'description' => TranslateController::t('Description'),
            'prise' => TranslateController::t('Prise'),
            'count' => TranslateController::t('Count'),
            'free' => TranslateController::t('Free'),
            'hotel_id' => TranslateController::t('Hotel'),
            'number_seats' => TranslateController::t('Number Seats'),
            'count_room' => TranslateController::t('Count Room'),
            'img' => TranslateController::t('Photo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelResidents()
    {
        return $this->hasMany(HotelResidents::className(), ['room_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['id' => 'hotel_id']);
    }
}
