<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;

/**
 * This is the model class for table "place_add_services".
 *
 * @property integer $id
 * @property string $place
 * @property integer $country_id
 * @property integer $city_id
 *
 * @property AddServices[] $addServices
 * @property CrmCityList $city
 * @property CrmCountryList $country
 */
class PlaceAddServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'place_add_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id'], 'required'],
            [['country_id', 'city_id'], 'integer'],
            [['place'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'place' => TranslateController::t('Place'),
            'country_id' => TranslateController::t('Country'),
            'city_id' =>TranslateController::t('City'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddServices()
    {
        return $this->hasMany(AddServices::className(), ['place_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(CityList::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(CountryList::className(), ['id' => 'country_id']);
    }
}
