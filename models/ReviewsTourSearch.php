<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReviewsTour;

/**
 * ReviewsTourSearch represents the model behind the search form about `app\models\ReviewsTour`.
 */
class ReviewsTourSearch extends ReviewsTour
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tur_id', 'user_id', 'create_at', 'update_at'], 'integer'],
            [['transport_type', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReviewsTour::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tur_id' => $this->tur_id,
            'user_id' => $this->user_id,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'transport_type', $this->transport_type])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
