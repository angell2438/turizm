<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tur;

/**
 * TurSearch represents the model behind the search form about `app\models\Tur`.
 */
class TurSearch extends Tur
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'prise', 'sale', 'long_time', 'visit', 'country_id', 'city_id'], 'integer'],
            [['name', 'description', 'price_type', 'type', 'img', 'accommodation', 'optional_activities', 'meals', 'meals_img', 'no_tip_necessary'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tur::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'prise' => $this->prise,
            'sale' => $this->sale,
            'long_time' => $this->long_time,
            'visit' => $this->visit,
            'country_id' => $this->country_id,
            'city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'price_type', $this->price_type])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'accommodation', $this->accommodation])
            ->andFilterWhere(['like', 'optional_activities', $this->optional_activities])
            ->andFilterWhere(['like', 'meals', $this->meals])
            ->andFilterWhere(['like', 'meals_img', $this->meals_img])
            ->andFilterWhere(['like', 'no_tip_necessary', $this->no_tip_necessary]);

        return $dataProvider;
    }
}
