<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;

/**
 * This is the model class for table "crm_city_list".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $title
 * @property string $state
 * @property string $region
 * @property integer $biggest_city
 *
 * @property CrmCountryList $country
 * @property DayForTur[] $dayForTurs
 * @property Hotel[] $hotels
 * @property PlaceAddServices[] $placeAddServices
 * @property Tur[] $turs
 */
class CityList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crm_city_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'biggest_city'], 'integer'],
            [['title'], 'required'],
            [['title', 'state', 'region'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => TranslateController::t('Country'),
            'title' => TranslateController::t('Title'),
            'state' => TranslateController::t('State'),
            'region' => TranslateController::t('Region'),
            'biggest_city' => TranslateController::t('Biggest City'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(CountryList::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDayForTurs()
    {
        return $this->hasMany(DayForTur::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotels()
    {
        return $this->hasMany(Hotel::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceAddServices()
    {
        return $this->hasMany(PlaceAddServices::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurs()
    {
        return $this->hasMany(Tur::className(), ['city_id' => 'id']);
    }

}
