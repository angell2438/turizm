<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $name
 * @property string $last_name
 * @property string $surname
 * @property string $email
 * @property string $pass
 * @property string $avatar
 * @property integer $birthday
 * @property integer $create_at
 * @property integer $update_at
 * @property integer $status
 * @property string $auth_key
 *
 * @property HotelResidents[] $hotelResidents
 * @property Reviews[] $reviews
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_NOT_ACTIVE = 1;

    public $pass_hash;

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }

    public static function findByUsername($username){
        return static::findOne([
            'username' => $username
        ]);
    }

    public function setPassword($password){
        $this ->pass = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey(){
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password,$this->pass);
    }

    public static function findIdentity($id)
    {
        return static::findOne(
            [
                'id' =>$id,
                'status'    => self::STATUS_ACTIVE
            ]
        );
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'last_name', 'surname', 'email', 'pass','username'], 'required'],
            [['birthday', 'create_at', 'update_at', 'status','role_id'], 'integer'],
            [['name', 'last_name', 'surname','username'], 'string', 'max' => 55],
            [['email', 'pass', 'avatar', 'auth_key'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => TranslateController::t('Nickname'),
            'name' => TranslateController::t('Name'),
            'last_name' => TranslateController::t('Last Name'),
            'surname' => TranslateController::t('Patronymic'),
            'email' => TranslateController::t('Email'),
            'pass' => TranslateController::t('Password'),
            'avatar' => TranslateController::t('Avatar'),
            'birthday' => TranslateController::t('Birthday'),
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'status' => TranslateController::t('Status'),
            'auth_key' => TranslateController::t('Auth Key'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelResidents()
    {
        return $this->hasMany(HotelResidents::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['user_id' => 'id']);
    }

    public function getReviewsTour()
    {
        return $this->hasMany(ReviewsTour::className(), ['user_id' => 'id']);
    }
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }
}
