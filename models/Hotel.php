<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;

/**
 * This is the model class for table "hotel".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $country_id
 * @property integer $city_id
 * @property integer $star
 * @property string $facilities
 * @property string $language_id
 * @property string $details_policies
 * @property string $area
 * @property string $area_coordinates
 * @property integer $discount
 *
 * @property CityList $city
 * @property CountryList $country
 * @property HotelResidents[] $hotelResidents
 * @property Reviews[] $reviews
 * @property RoomType[] $roomTypes
 */
class Hotel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country_id'], 'required'],
            [['country_id', 'city_id', 'star', 'discount'], 'integer'],
            [['details_policies', 'area', 'area_coordinates'], 'string'],
            [['name', 'description', 'language_id','img'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => TranslateController::t('Name'),
            'description' => TranslateController::t('Description'),
            'country_id' => TranslateController::t('Country'),
            'city_id' => TranslateController::t('City'),
            'star' => TranslateController::t('Star'),
            'facilities' => TranslateController::t('Facilities'),
            'language_id' => TranslateController::t('Languages'),
            'details_policies' => TranslateController::t('Details Policies'),
            'area' =>TranslateController::t('Area'),
            'area_coordinates' => TranslateController::t('Area Coordinates'),
            'discount' =>TranslateController::t('sale'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(CityList::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(CountryList::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelResidents()
    {
        return $this->hasMany(HotelResidents::className(), ['hotel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['hotel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomTypes()
    {
        return $this->hasMany(RoomType::className(), ['hotel_id' => 'id']);
    }
}
