<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "day_for_tur".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $country_id
 * @property integer $city_id
 * @property integer $tur_id
 *
 * @property CityList $city
 * @property CountryList $country
 * @property Tur $tur
 */
class DayForTur extends ActiveRecord
{


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'day_for_tur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country_id','number_day'], 'required'],
            [['country_id', 'city_id', 'tur_id', 'create_at', 'update_at','number_day'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => TranslateController::t('Name'),
            'description' => TranslateController::t('Description'),
            'country_id' => TranslateController::t('Country'),
            'city_id' => TranslateController::t('City'),
            'tur_id' => TranslateController::t('Tur'),
            'number_day' => TranslateController::t('Number of the day'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(CityList::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(CountryList::className(), ['id' => 'country_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTur()
    {
        return $this->hasOne(Tur::className(), ['id' => 'tur_id']);
    }
}
