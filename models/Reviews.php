<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $description
 * @property integer $hotel_id
 * @property integer $plus
 * @property integer $minus
 *
 * @property Hotel $hotel
 * @property User $user
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'hotel_id', 'plus', 'minus'], 'integer'],
            [['description'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => TranslateController::t('User'),
            'description' => TranslateController::t('Description'),
            'hotel_id' => TranslateController::t('Hotel'),
            'plus' => TranslateController::t('Plus'),
            'minus' => TranslateController::t('Minus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
