<?php

namespace app\models;

use app\commands\TranslateController;
use Yii;

/**
 * This is the model class for table "tur".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $prise
 * @property string $price_type
 * @property string $type
 * @property integer $sale
 * @property integer $long_time
 * @property integer $visit
 * @property integer $country_id
 * @property integer $city_id
 * @property string $img
 * @property string $accommodation
 * @property string $optional_activities
 * @property string $meals
 * @property string $meals_img
 * @property string $no_tip_necessary
 *
 * @property AddServices[] $addServices
 * @property DayForTur[] $dayForTurs
 * @property CityList $city
 * @property CountryList $country
 */
class Tur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price_type', 'country_id','prise'], 'required'],
            [['description', 'accommodation', 'optional_activities', 'meals', 'meals_img', 'no_tip_necessary','img','main_img'], 'string'],
            [['prise',  'long_time', 'visit', 'country_id', 'city_id','continent_id'], 'integer'],
            //[['start','end'], 'date', 'format' => 'yyyy-M-d H:m:s'],
            [['name'], 'string', 'max' => 255],
            [['sale'], 'integer', 'max' => 100],
            [['price_type'], 'string', 'max' => 55],
            [['type'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => TranslateController::t('Name'),
            'description' =>TranslateController::t('Description'),
            'prise' => TranslateController::t('Prise'),
            'price_type' => TranslateController::t('Prise for'),
            'type' => TranslateController::t('Type'),
            'sale' => TranslateController::t('Sale'),
            'long_time' => TranslateController::t('Duration'),
            'visit' => TranslateController::t('Visits'),
            'country_id' => TranslateController::t('Country'),
            'city_id' => TranslateController::t('City'),
            'img' => TranslateController::t('Photo'),
            'main_img' => TranslateController::t('Main Photo'),
            'accommodation' => TranslateController::t('Accommodation'),
            'optional_activities' => TranslateController::t('Optional Activities'),
            'meals' => TranslateController::t('Meals'),
            'meals_img' =>TranslateController::t('Meals Images'),
            'no_tip_necessary' => TranslateController::t('No Tip Necessary'),
            'continent_id ' => TranslateController::t('Continent'),
            'start ' => TranslateController::t('Start'),
            'end ' => TranslateController::t('End'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddServices()
    {
        return $this->hasMany(AddServices::className(), ['tur_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDayForTurs()
    {
        return $this->hasMany(DayForTur::className(), ['tur_id' => 'id']);
    }

    public function getReviewsTour()
    {
        return $this->hasMany(ReviewsTour::className(), ['tur_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(CityList::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(CountryList::className(), ['id' => 'country_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContinent()
    {
        return $this->hasOne(Continent::className(), ['id' => 'continent_id']);
    }
}
