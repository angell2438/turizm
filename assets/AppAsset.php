<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/library/font-awesome.min.css',
        'css/library/bootstrap.min.css',
        'css/library/jquery-ui.min.css',
        'css/library/owl.carousel.css',
        'css/library/jquery.mb.YTPlayer.min.css',
        'css/style.css',
        'admin-style/bootstrap/css/bootstrap-responsive.min.css',
        'admin-style/vendors/easypiechart/jquery.easy-pie-chart.css',
        'admin-style/assets/styles.css',
        'datepiker/css/bootstrap-datepicker.min.css',


        'http://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:300,400,600',
    ];
    public $js = [
        'http://maps.google.com/maps/api/js?sensor=false',
        'js/library/jquery-ui.min.js',
        'js/library/owl.carousel.min.js',
        'js/library/parallax.min.js',
        'js/library/jquery.nicescroll.js',
        'js/library/jquery.ui.touch-punch.min.js',
        'js/library/jquery.mb.YTPlayer.min.js',
        'js/library/SmoothScroll.js',
        'js/script.js',
        'js/my_script.js',
        'datepiker/js/bootstrap-datepicker.min.js',
        'datepiker/locales/bootstrap-datepicker.ru.min.js',
        'datepiker/locales/bootstrap-datepicker.uk.min.js',
        'admin-style/js/main.js',
        'admin-style/vendors/modernizr-2.6.2-respond-1.1.0.min.js',
        'http://html5shim.googlecode.com/svn/trunk/html5.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
