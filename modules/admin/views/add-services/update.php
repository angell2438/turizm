<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AddServices */
/* @var $tur app\models\Tur */
/* @var $res app\models\PlaceAddServices */

$this->title = 'Update Add Services: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Add Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="add-services-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tur'   => $tur,
        'res'   => $res
    ]) ?>

</div>
