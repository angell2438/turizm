<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AddServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Add Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add-services-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Add Services', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header'=>'№'
            ],

            'id',
            'name',
            'description',
            'price',
            'price_type',
            [
                'attribute'=>'tur_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->tur->name;
                }
            ],
            [
                'attribute'=>'place_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->place->country->title.' - '.$model->place->place.', '.$model->place->city->state;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
