<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AddServices */
/* @var $tur app\models\Tur */
/* @var $res app\models\PlaceAddServices */

$this->title = 'Create Add Services';
$this->params['breadcrumbs'][] = ['label' => 'Add Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add-services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tur'   =>  $tur,
        'res'   => $res
    ]) ?>

</div>
