<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlaceAddServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Place Add Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="place-add-services-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Place Add Services', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'№'],

            'place',
            [
                'attribute'=>'country_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->country->title;
                }
            ],
            [
                'attribute'=>'city_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->city->title;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
