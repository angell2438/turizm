<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlaceAddServices */

$this->title = 'Create Place Add Services';
$this->params['breadcrumbs'][] = ['label' => 'Place Add Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="place-add-services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
