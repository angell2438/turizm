<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReviewsTourSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reviews Tours';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reviews-tour-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<p>
        <?/*= Html::a('Create Reviews Tour', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'№'],

            [
                'attribute'=>'tur_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->tur->name;
                }
            ],
            [
                'attribute'=>'user_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->user->username;
                }
            ],
            'transport_type',
            'description:ntext',
        ],
    ]); ?>

</div>
