<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DayForTur */

$this->title = 'Create Day For Tur';
$this->params['breadcrumbs'][] = ['label' => 'Day For Turs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="day-for-tur-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'country' =>$country,
        'city' =>$city,
        'tur' =>$tur,
    ]) ?>

</div>
