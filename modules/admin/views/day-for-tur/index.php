<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DayForTurSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Day For Turs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="day-for-tur-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Day For Tur', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header' =>'№'],

            'name',
            'description',
            [
                'attribute'=>'country_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->country->title;
                }
            ],
            [
                'attribute'=>'city_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return (!empty($city=$model->city->title))?$city:\app\commands\TranslateController::t('City is missing');
                }
            ],
            [
                'attribute'=>'tur_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->tur->name;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
