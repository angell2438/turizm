<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DayForTur */

$this->title = 'Update Day For Tur: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Day For Turs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="day-for-tur-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'country' =>$country,
        'city' =>$city,
        'tur' =>$tur,
    ]) ?>

</div>
