<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DayForTur */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Day For Turs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="day-for-tur-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'number_day',
            'name',
            'description',
            'country_id',
            'city_id',
            'tur_id',

        ],
    ]) ?>

</div>
