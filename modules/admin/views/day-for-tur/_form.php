<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DayForTur */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="day-for-tur-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number_day')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'country_id')->dropDownList($country) ?>

    <?= $form->field($model, 'city_id')->dropDownList($city) ?>

    <?= $form->field($model, 'tur_id')->dropDownList($tur) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
