<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CityList */
/* @var $country app\models\CountryList */

$this->title = 'Create City List';
$this->params['breadcrumbs'][] = ['label' => 'City Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'country' => $country
    ]) ?>

</div>
