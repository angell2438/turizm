<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\commands\TranslateController;
/* @var $this yii\web\View */
/* @var $model app\models\CityList */
/* @var $country  */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'country_id')->dropDownList($country) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'biggest_city')->dropDownList([TranslateController::t('Small City'),TranslateController::t('Big City')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
