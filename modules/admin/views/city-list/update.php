<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CityList */
/* @var $country app\models\CountryList */

$this->title = 'Update City List: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'City Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="city-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'country' => $country
    ]) ?>

</div>
