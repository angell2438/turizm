<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CityListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'City Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create City List', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header' =>'№'],
            [
                'attribute'=>'country_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->country->title;
                }
            ],
            'title',
            'state',
            'region',
            // 'biggest_city',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
