<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReviewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reviews-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<p>
        <?/*= Html::a('Create Reviews', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'user_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->user->username;
                }
            ],
            'description:ntext',
            [
                'attribute'=>'hotel_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->hotel->name;
                }
            ],
            'plus',
            'minus',

        ],
    ]); ?>

</div>
