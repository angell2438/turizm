<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\commands\TranslateController;
AppAsset::register($this);
$this->title = Yii::t("Admin Panel","Admin Panel")
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-md-2" id="sidebar">
        <?php
                echo Nav::widget([
                    'options' => ['class' => ''],
                    'items' => [
                        ['label' => TranslateController::t('Tours'), 'url' => ['/admin/tur/index']],
                        ['label' => TranslateController::t('County'), 'url' => ['/admin/country-list/index']],
                        ['label' => TranslateController::t('City'), 'url' => ['/admin/city-list/index']],
                        ['label' => TranslateController::t('Hotel'), 'url' => ['/admin/hotel/index']],
                        ['label' => TranslateController::t('Room type'), 'url' => ['/admin/room-type/index']],
                        ['label' => TranslateController::t('Tour day'), 'url' => ['/admin/day-for-tur/index']],
                        ['label' => TranslateController::t('Additional services'), 'url' => ['/admin/add-services/index']],
                        ['label' => TranslateController::t('Evaluation Criteria hotels'), 'url' => ['/admin/criterion/index']],
                        ['label' => TranslateController::t('Visitors hotels'), 'url' => ['/admin/hotel-residents/index']],
                        ['label' => TranslateController::t('Locations of additional service'), 'url' => ['/admin/place-add-services/index']],
                        ['label' => TranslateController::t('Hotel Reviews'), 'url' => ['/admin/reviews/index']],
                        ['label' => TranslateController::t('Users'), 'url' => ['/admin/user/index']],
                        ['label' => TranslateController::t('Translate'), 'url' => ['/admin/translate/index']],
                        ['label' => TranslateController::t('Reviews Tour'), 'url' => ['/admin/reviews-tour']],
                        ['label' => TranslateController::t('Role'), 'url' => ['/admin/role']],
                    ],
                ]);
                ?>
        </div>

        <!--/span-->
        <div class="span9" id="content">
            <?= Breadcrumbs::widget([
                'homeLink' => ['label' => 'Admin',
                    'url' => '/admin/' ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>
    <hr>
</div>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>