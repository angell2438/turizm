<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="preloader">
    <div class="tb-cell">
        <div id="page-loading">
            <div></div>
            <p>Loading</p>
        </div>
    </div>
</div>
<div class="wrap">
    <?php
    /*    NavBar::begin([
            'brandLabel' => 'My Company',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'About', 'url' => ['/site/about']],
                ['label' => 'Contact', 'url' => ['/site/contact']],
                Yii::$app->user->isGuest ? (
                    ['label' => 'Login', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
        NavBar::end();
        */?>

    <header id="header" class="header">
        <div class="container">

            <!-- Logo -->
            <div class="logo float-left">
                <?= Html::a('<img src="'.Url::to("images/logo-header2.png").'" alt="">',['/site/index'])?>
            </div>
            <!-- End Logo -->
            <!-- Bars -->
            <div class="bars" id="bars"></div>
            <!-- End Bars -->

            <!--Navigation-->
            <nav class="navigation nav-c" id="navigation" data-menu-type="1200">
                <div class="nav-inner">
                    <?=Html::a('Close',['#'],['class'=>'bars-close','id'=>'bars-close'])?>
                    <div class="tb">
                        <div class="tb-cell">
                            <ul class="menu-list text-uppercase">
                                <li class="current-menu-parent">
                                    <?=Html::a('Главная',['/site/index'])?>
                                </li>
                                <li>
                                    <?=Html::a('Сторінки')?>
                                    <ul class="sub-menu">
                                        <li>
                                            <?=Html::a('Про компанію',['/site/about'])?>
                                        </li>
                                        <li>
                                            <?=Html::a('Про компанію',['/site/user-singup'])?>
                                        </li>
                                        <li>
                                            <?=Html::a('Контакт',['/site/contact'])?>
                                        </li>
                                        <li>
                                            <?=Html::a('Оплата',['/site/payment'])?>
                                        </li>
                                        <!--                                            <li><a href="element.html" title="">елементи</a></li>-->
                                        <li>
                                            <?=Html::a('404',['/site/404'])?>
                                        </li>
                                        <li>
                                            <?=Html::a('Повернетесь',['/site/comingsoon'])?>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <?=Html::a('Готель',['/site/home-hotel'])?>
                                </li>
                                <li>
                                    <?=Html::a('Переліт',['/site/flight-list'])?>
                                </li>
                                <li>
                                    <?=Html::a('Автобуси',['/site/cart'])?>

                                </li>
                                <li>
                                    <?=Html::a('Пакети',['/site/package-list'])?>
                                </li>
                                <li>
                                    <?=Html::a('Круізи',['/site/cruise-list'])?>

                                </li>
                                <li>
                                    <?=Html::a('Тури',['/site/tour-list'])?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--End Navigation-->
        </div>
    </header>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>
<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <!-- Logo -->
            <div class="col-md-4">
                <div class="logo-foter">
                    <?= Html::a('<img src="'.Url::to("images/logo-header2.png").'" alt="">',['/site/index'])?>
                </div>
            </div>
            <!-- End Logo -->
            <!-- Navigation Footer -->
            <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="ul-ft">
                    <ul>
                        <li><?=Html::a('Про компанію',['/site/about'])?></li>
                        <li><?=Html::a('Блог',['/site/blog'])?></li>
                        <li><?=Html::a('FQA',['/site/fqa'])?></li>
                        <li><?=Html::a('Carrers',['/site/careers'])?></li>
                    </ul>
                </div>
            </div>
            <!-- End Navigation Footer -->
            <!-- Navigation Footer -->
            <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="ul-ft">
                    <ul>
                        <li><?=Html::a('Свяжитесь с нами',['/site/contact'])?></li>
                        <li><?=Html::a('Конфиденциальность')?></li>
                        <li><?=Html::a('Срок службы')?></li>
                        <li><?=Html::a('Безопасность')?></li>
                    </ul>
                </div>
            </div>
            <!-- End Navigation Footer -->
            <!-- Footer Currency, Language -->
            <div class="col-sm-6 col-md-4">
                <!-- Language -->
                <div class="currency-lang-bottom dropdown-cn float-left">
                    <div class="dropdown-head">
                        <span class="angle-down"><i class="fa fa-angle-down"></i></span>
                    </div>
                    <div class="dropdown-body">
                        <ul>
                            <li class="current"><a href="#" title="">Ukrainian</a></li>
                            <li><a href="#" title="">English</a></li>
                            <li><a href="#" title="">Russian</a></li>
                            <li><a href="#" title="">Deutsch</a></li>
                            <li><a href="#" title="">Francais</a></li>
                            <li><a href="#" title="">Italiano</a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Language -->
                <!-- Currency -->
                <div class="currency-lang-bottom dropdown-cn float-left">
                    <div class="dropdown-head">
                        <span class="angle-down"><i class="fa fa-angle-down"></i></span>
                    </div>
                    <div class="dropdown-body">
                        <ul>
                            <li class="current"><a href="#" title="">US</a></li>
                            <li><a href="#" title="">Rus</a></li>
                            <li><a href="#" title="">Uk</a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Currency -->
                <!--CopyRight-->
                <p class="copyright">
                    © 2009 – 2014 Bookyourtrip™ All rights reserved.
                </p>
                <!--CopyRight-->
            </div>
            <!-- End Footer Currency, Language -->
        </div>
    </div>
</footer>
<!-- End Footer -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
