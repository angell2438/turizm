<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TurSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Turs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tur-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tur', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header' => '№'],

            //'id',
            'name',
            'description:ntext',
            'prise',
            'price_type',
            // 'type',
            // 'sale',
            // 'long_time:datetime',
            // 'visit',
            // 'country_id',
            // 'city_id',
            // 'img',
            // 'accommodation:ntext',
            // 'optional_activities:ntext',
            // 'meals:ntext',
            // 'meals_img:ntext',
            // 'no_tip_necessary:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
