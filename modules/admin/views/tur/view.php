<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tur */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Turs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tur-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'prise',
            'price_type',
            'type',
            'sale',
            'long_time:datetime',
            'country_id',
            'city_id',
            'img',
            'main_img',
            'accommodation:ntext',
            'optional_activities:ntext',
            'meals:ntext',
            'meals_img:ntext',
            'no_tip_necessary:ntext',
        ],
    ]) ?>

</div>
