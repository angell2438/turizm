<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\commands\TranslateController;
/* @var $this yii\web\View */
/* @var $model app\models\Tur */
/* @var $country app\models\CountryList */
/* @var $city app\models\CityList */
/* @var $form yii\widgets\ActiveForm */

$lang =\app\models\Lang::getCurrent();
$lang= $lang->url;
?>

<div class="tur-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'prise')->textInput() ?>

    <?= $form->field($model, 'price_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'start')->textInput() ?>
    <?= $form->field($model, 'end')->textInput() ?>

    <?= $form->field($model, 'sale')->textInput() ?>

    <?= $form->field($model, 'long_time')->textInput() ?>


    <?= $form->field($model, 'continent_id')->dropDownList($continent) ?>

    <?= $form->field($model, 'country_id')->dropDownList($country) ?>

    <?= $form->field($model, 'city_id')->dropDownList($city) ?>

    <?php
    if(!empty($model->img)){
        $images = explode(';',$model->img);
        foreach($images as $image ){
            if(!empty($image)){?>
                <div class="admin-img-cont col-md-3">
                    <div data-url="<?=$image?>" data-from="img" class="close"  data-model="<?=Yii::$app->controller->id?>">x</div>
                    <?=Html::img('/'.$image,['class'=>'col-md-12']);?>
                </div>
                <?php
            }
        }
    }
    ?>
    <div class="clear-fix"></div>


    <?= $form->field($upload, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label(TranslateController::t('Photo')) ?>

    <?php
    if(!empty($model->main_img)){
        $images = explode(';',$model->main_img);
        foreach($images as $image ){
            if(!empty($image)){?>
                <div class="admin-img-cont col-md-3">
                    <div data-url="<?=$image?>" data-from="main_img" class="close"  data-model="<?=Yii::$app->controller->id?>">x</div>
                    <?=Html::img('/'.$image,['class'=>'col-md-12']);?>
                </div>
                <?php
            }
        }
    }
    ?>
    <div class="clear-fix"></div>
    <?= $form->field($upload, 'imageFilesMain[]')->fileInput(['accept' => 'image/*'])->label(TranslateController::t('Main Photo')) ?>

    <?= $form->field($model, 'accommodation')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'optional_activities')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'meals')->textarea(['rows' => 6]) ?>
    <?php
    if(!empty($model->meals_img)){
        $images = explode(';',$model->meals_img);
        foreach($images as $image ){
            if(!empty($image)){?>
                <div class="admin-img-cont col-md-3">
                    <div data-url="<?=$image?>" data-from="meals_img" class="close"  data-model="<?=Yii::$app->controller->id?>">x</div>
                    <?=Html::img('/'.$image,['class'=>'col-md-12']);?>
                </div>
    <?php
            }
        }
    }
    ?>
    <div class="clear-fix"></div>
    <?= $form->field($upload, 'imageFilesMeal[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label(TranslateController::t('Meal Photo')) ?>

    <?= $form->field($model, 'no_tip_necessary')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <script>
        $('#tur-start,#tur-end').datepicker({
            language: "<?=$lang?>",
            format: "mm/d/yyyy"
        });
    </script>

</div>
