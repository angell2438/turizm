<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\commands\TranslateController;
/* @var $this yii\web\View */
/* @var $model app\models\Hotel */
/* @var $country app\models\CountryList */
/* @var $city app\models\CityList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hotel-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php
    if(!empty($model->img)){
        $images = explode(';',$model->img);
        foreach($images as $image ){
            if(!empty($image)){?>
                <div class="admin-img-cont col-md-3">
                    <div data-url="<?=$image?>" data-from="img" class="close"  data-model="<?=Yii::$app->controller->id?>">x</div>
                    <?=Html::img('/'.$image,['class'=>'col-md-12']);?>
                </div>
                <?php
            }
        }
    }
    ?>
    <div class="clear-fix"></div>
    <?= $form->field($upload, 'imageFilesMain[]')->fileInput(['accept' => 'image/*'])->label(TranslateController::t('Main Photo')) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country_id')->dropDownList($country) ?>

    <?= $form->field($model, 'city_id')->dropDownList($city) ?>

    <?= $form->field($model, 'star')->textInput() ?>

    <?= $form->field($model, 'facilities')->dropDownList($facilities,['multiple' => true , 'style' => 'width:400px;']) ?>



    <?= $form->field($model, 'language_id')->textInput(['maxlength' => true])->input('input',['placeholder' => TranslateController::t("Separate language ','")]) ?>

    <?= $form->field($model, 'details_policies')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'area')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'area_coordinates')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'discount')->textInput() ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <script>
        $(document).ready(function(){
            <?php
            $facilities = explode(';',$model->facilities);
            foreach ($facilities as $item) {
                $fun ="$('#hotel-facilities option[value=\"$item\"]').attr('selected',true);\n";
                echo "$fun";
            }
            ?>
        });
    </script>
</div>
