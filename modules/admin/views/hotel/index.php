<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HotelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hotels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Hotel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'№'],

            'name',
            'description',
            [
                'attribute'=>'country_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->country->title;
                }
            ],
            [
                'attribute'=>'city_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return (!empty($city=$model->city->title))?$city:\app\commands\TranslateController::t('City is missing');
                }
            ],
            // 'star',
            // 'facilities',
            // 'language_id',
            // 'details_policies:ntext',
            // 'area:ntext',
            // 'area_coordinates:ntext',
            // 'discount',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
