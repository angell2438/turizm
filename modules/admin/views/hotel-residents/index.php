<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HotelResidentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hotel Residents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-residents-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--<p>
        <?/*= Html::a('Create Hotel Residents', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'user_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    $user= $model->user->last_name.' '.$model->user->name.' '.$model->user->surname;
                    return $user;
                }
            ],
            [
                'attribute'=>'hotel_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->hotel->name;
                }
            ],
            [
                'attribute'=>'room_type_id',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($model){
                    return $model->roomType->name;
                }
            ],
            'check_in',
            'check_out',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
