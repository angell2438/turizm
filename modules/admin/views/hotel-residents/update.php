<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HotelResidents */

$this->title = 'Update Hotel Residents: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hotel Residents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hotel-residents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
