<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HotelResidents */

$this->title = 'Create Hotel Residents';
$this->params['breadcrumbs'][] = ['label' => 'Hotel Residents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-residents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
