<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HotelResidents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hotel-residents-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput(['value'=>$model->user->username]) ?>

    <?= $form->field($model, 'hotel_id')->textInput(['value'=>$model->hotel->name]) ?>

    <?= $form->field($model, 'room_type_id')->textInput(['value'=>$model->roomType->name]) ?>

    <?= $form->field($model, 'check_in')->textInput(['class' => 'input-group date']) ?>
    <div class="form-field field-date">
        <input type="text" class="field-input calendar-input hasDatepicker" name="start" placeholder="Start date" id="dp1463332733551">
    </div>


    <?= $form->field($model, 'check_out')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
