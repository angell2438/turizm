<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\commands\TranslateController;
/* @var $this yii\web\View */
/* @var $model app\models\RoomType */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="room-type-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'hotel_id')->dropDownList($hotel) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'prise')->textInput() ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <?= $form->field($model, 'free')->textInput() ?>

    <?= $form->field($model, 'number_seats')->textInput() ?>

    <?= $form->field($model, 'count_room')->textInput() ?>

    <?php
    if(!empty($model->img)){
        $images = explode(';',$model->img);
        foreach($images as $image ){
            if(!empty($image)){?>
                <div class="admin-img-cont col-md-3">
                    <div data-url="<?=$image?>" data-from="img" class="close" data-model="<?=Yii::$app->controller->id?>">x</div>
                    <?=Html::img('/'.$image,['class'=>'col-md-12']);?>
                </div>
                <?php
            }
        }
    }
    ?>
    <div class="clear-fix"></div>
    <?= $form->field($upload, 'imageFiles[]')->fileInput(['multiple' => true,'accept' => 'image/*'])->label(TranslateController::t('Photo')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
