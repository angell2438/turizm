<?php

namespace app\modules\admin\controllers;

use app\models\CityList;
use app\models\CountryList;
use app\models\Tur;
use Yii;
use app\models\DayForTur;
use app\models\DayForTurSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DayForTurController implements the CRUD actions for DayForTur model.
 */
class DayForTurController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DayForTur models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DayForTurSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DayForTur model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DayForTur model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DayForTur();
        $country = CountryList::find()->all();
        $country = ArrayHelper::map($country,'id','title');
        $city = ArrayHelper::map(CityList::find()
            ->where('country_id=1')
            ->all(),'id','title');
        $tur = ArrayHelper::map(Tur::find()->all(),'id','name');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'country' =>$country,
                'city' =>$city,
                'tur' =>$tur,
            ]);
        }
    }

    /**
     * Updates an existing DayForTur model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $country = CountryList::find()->all();
        $country = ArrayHelper::map($country,'id','title');
        $city = ArrayHelper::map(CityList::find()
            ->where(['country_id'=>$model->country_id])
            ->all(),'id','title');
        $tur = ArrayHelper::map(Tur::find()->all(),'id','name');


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'country' =>$country,
                'city' =>$city,
                'tur' =>$tur,
            ]);
        }
    }

    /**
     * Deletes an existing DayForTur model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DayForTur model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DayForTur the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DayForTur::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
