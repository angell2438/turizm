<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\AddServices;
use app\models\AddServicesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Tur;
use yii\helpers\ArrayHelper;
use app\models\PlaceAddServicesSearch;
/**
 * AddServicesController implements the CRUD actions for AddServices model.
 */
class AddServicesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AddServices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AddServicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AddServices model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AddServices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AddServices();
        $tur = Tur::find()->select(['id','name'])->all();
        $tur = ArrayHelper::map($tur,'id','name');
        $place = PlaceAddServicesSearch::find()->all();
        $res =[];
        foreach($place as $p){
            $res[$p->id] = $p->country->title.' - '.$p->place.', '.$p->city->state;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'tur'   => $tur,
                'res'   => $res
            ]);
        }
    }

    /**
     * Updates an existing AddServices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tur = Tur::find()->select(['id','name'])->all();
        $tur = ArrayHelper::map($tur,'id','name');
        $place = PlaceAddServicesSearch::find()->all();
        $res =[];
        foreach($place as $p){
            $res[$p->id] = $p->country->title.' - '.$p->place.', '.$p->city->state;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'tur'   => $tur,
                'res'   => $res
            ]);
        }
    }

    /**
     * Deletes an existing AddServices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AddServices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AddServices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AddServices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
