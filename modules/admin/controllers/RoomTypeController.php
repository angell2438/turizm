<?php

namespace app\modules\admin\controllers;

use app\models\Hotel;
use app\models\UploadForm;
use Yii;
use app\models\RoomType;
use app\models\RoomTypeSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * RoomTypeController implements the CRUD actions for RoomType model.
 */
class RoomTypeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RoomType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoomTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RoomType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RoomType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RoomType();

        $hotel = ArrayHelper::map(Hotel::find()->all(),'id','name');
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $upload->imageFiles = UploadedFile::getInstances($upload, 'imageFiles');
            //echo "<pre>".print_r($upload,true)."<pre>";die();
            $path = 'uploads';
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/hotel';
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/'.$model->hotel->id;
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/room';
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/'.$model->id;
            if( !file_exists($path) ){
                mkdir($path);
            }
            foreach ($upload->imageFiles as $file) {
                $file_name = $path . '/' . md5($file->baseName) . '.' . $file->extension;
                if( !file_exists($file_name) ){
                    if($file->saveAs($file_name)){
                        $model->img .= $file_name.';';
                    }
                }
            }
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'hotel'=>$hotel,
                'upload'=>$upload
            ]);
        }
    }

    /**
     * Updates an existing RoomType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $hotel = ArrayHelper::map(Hotel::find()->all(),'id','name');
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $upload->imageFiles = UploadedFile::getInstances($upload, 'imageFiles');
            //echo "<pre>".print_r($upload,true)."<pre>";die();
            $path = 'uploads';
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/hotel';
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/'.$model->hotel->id;
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/room';
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/'.$model->id;
            if( !file_exists($path) ){
                mkdir($path);
            }
            foreach ($upload->imageFiles as $file) {
                $file_name = $path . '/' . md5($file->baseName) . '.' . $file->extension;
                if( !file_exists($file_name) ){
                    if($file->saveAs($file_name)){
                        $model->img .= $file_name.';';
                    }
                }
            }
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'hotel'=>$hotel,
                'upload'=>$upload
            ]);
        }
    }

    /**
     * Deletes an existing RoomType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RoomType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RoomType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RoomType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionDeleteImg(){
        $id=$_POST['id'];
        $from = $_POST['from'];
        if($url = $_POST['url']){
            $models = $this->findModel($id);
            $images = explode(';',$models->$from);
            foreach ($images as $key =>$value ) {
                if($url ===$value){
                    if(file_exists($value))unlink($value);
                    unset($images[$key]);
                }
            }
            $images = implode(';',$images);
            $models->$from = $images;
            return($models->save())?true:false;
        }
        return false;
    }
}
