<?php

namespace app\modules\admin\controllers;

use app\models\Facilities;
use app\models\UploadForm;
use Yii;
use app\models\Hotel;
use app\models\HotelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\CityList;
use app\models\CountryList;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * HotelController implements the CRUD actions for Hotel model.
 */
class HotelController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Hotel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HotelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Hotel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Hotel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Hotel();
        $country = CountryList::find()->all();
        $country = ArrayHelper::map($country,'id','title');
        $city = ArrayHelper::map(CityList::find()
            ->where('country_id=1')
            ->all(),'id','title');
        $facilities = ArrayHelper::map(Facilities::find()->all(),'id','name');
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->facilities = implode($_POST['Hotel']['facilities'],';');
            $upload->imageFilesMain = UploadedFile::getInstances($upload, 'imageFilesMain');
            //echo "<pre>".print_r($upload,true)."<pre>";die();
            $path = 'uploads';
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/hotel';
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/'.$model->id;
            if( !file_exists($path) ){
                mkdir($path);
            }
            foreach ($upload->imageFilesMain as $file) {
                $file_name = $path . '/' . md5($file->baseName) . '.' . $file->extension;
                if( !file_exists($file_name) ){
                    if($file->saveAs($file_name)){
                        $model->img = $file_name;
                    }
                }
            }
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'country' => $country,
                'city'    => $city,
                'facilities' => $facilities,
                'upload'=>$upload
            ]);
        }
    }

    /**
     * Updates an existing Hotel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $country = CountryList::find()->all();
        $country = ArrayHelper::map($country,'id','title');
        $city = ArrayHelper::map(CityList::find()
            ->where(['country_id'=>$model->country_id])
            ->all(),'id','title');
        $upload = new UploadForm();
        $facilities = ArrayHelper::map(Facilities::find()->all(),'id','name');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->facilities = implode($_POST['Hotel']['facilities'],';');
            $upload->imageFilesMain = UploadedFile::getInstances($upload, 'imageFilesMain');
            //echo "<pre>".print_r($upload,true)."<pre>";die();
            $path = 'uploads';
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/hotel';
            if( !file_exists($path) ){
                mkdir($path);
            }
            $path .= '/'.$model->id;
            if( !file_exists($path) ){
                mkdir($path);
            }
            foreach ($upload->imageFilesMain as $file) {
                $file_name = $path . '/' . md5($file->baseName) . '.' . $file->extension;
                if( !file_exists($file_name) ){
                    if($file->saveAs($file_name)){
                        $model->img = $file_name;
                    }
                }
            }
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'country' => $country,
                'city'    => $city,
                'facilities' => $facilities,
                'upload'=>$upload
            ]);
        }
    }

    /**
     * Deletes an existing Hotel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Hotel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hotel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Hotel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteImg(){
        $id=$_POST['id'];
        $from = $_POST['from'];
        if($url = $_POST['url']){
            $models = $this->findModel($id);
            $images = explode(';',$models->$from);
            foreach ($images as $key =>$value ) {
                if($url ===$value){
                    if(file_exists($value))unlink($value);
                    unset($images[$key]);
                }
            }
            $images = implode(';',$images);
            $models->$from = $images;
            return($models->save())?true:false;
        }
        return false;
    }
}
