<?php

/* @var $this yii\web\View */
/* @var $hot app\models\Tur */
/* @var $recomends app\models\Tur */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use app\commands\TranslateController;
$this->title = TranslateController::t('Home');
?>
<div id="wrap" style="margin-top: 0 !important;">

    <!--Banner-->
    <section class="banner">

        <!--Background-->
        <div class="bg-parallax bg-1"></div>
        <!--End Background-->

        <div class="container">

            <div class="logo-banner text-center">
                <a href="" title="">
                    <?=Html::img('/images/logo-banner.png')?>
                </a>
            </div>

            <!-- Banner Content -->
            <div class="banner-cn">

                <!-- Tabs Cat Form -->
                <ul class="tabs-cat text-center row">
                    <li class="cate-item col-xs-2">
                        <a data-toggle="tab" href="#form-flight" title="">
                            <span><?=TranslateController::t('Flight')?></span>
                            <?= Html::img('/images/icon-flight.png');?>
                        </a>
                    </li>
                    <li class="cate-item active col-xs-2">
                        <a data-toggle="tab" href="#form-hotel" title="">
                            <span><?=TranslateController::t('Hotel')?></span>
                            <?= Html::img('/images/icon-hotel.png');?>
                        </a>
                    </li>
                    <li class="cate-item col-xs-2">
                        <a data-toggle="tab" href="#form-car" title="">
                            <span><?=TranslateController::t('Bus')?></span>
                            <?= Html::img('/images/icon-car.png');?>
                        </a>
                    </li>
                    <li class="cate-item col-xs-2">
                        <a data-toggle="tab" href="#form-package" title="">
                            <span><?=TranslateController::t('Specials')?><!--Спец предложения--></span>
                            <?= Html::img('/images/icon-tour.png')?>
                        </a>
                    </li>
                    <li class="cate-item col-xs-2">
                        <a data-toggle="tab" href="#form-tour" title="">
                            <span><?=TranslateController::t('Tour')?></span>
                            <?= Html::img('/images/icon-vacation.png')?>
                        </a>
                    </li>
                </ul>
                <!-- End Tabs Cat -->

                <!-- Tabs Content -->
                <div class="tab-content">

                    <!-- Search Hotel -->
                    <div class="form-cn form-hotel tab-pane active in" id="form-hotel" role="tabpanel">
                        <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                        <div class="form-search clearfix">
                            <form>
                                <div class="form-field field-destination">
                                    <label for="destination"><span><?=TranslateController::t('Purpose:')?></span> <?=TranslateController::t('Country')?>, <?=TranslateController::t('Region')?>, <?=TranslateController::t('City')?></label>
                                    <input type="text" id="destination" class="field-input" name="place">
                                </div>
                                <div class="form-field field-date">
                                    <input type="text" class="field-input calendar-input"  name="start" placeholder="<?=TranslateController::t('Start date')?>">
                                </div>
                                <div class="form-field field-date">
                                    <input type="text" class="field-input calendar-input" name="end" placeholder="<?=TranslateController::t('End date')?>">
                                </div>
                                <div class="form-field field-select">
                                    <div class="select">
                                        <span><?=TranslateController::t('Number of seats')?></span>
                                        <select name="num_seat">
                                            <option value="1"><?=TranslateController::t('One')?></option>
                                            <option value="2"><?=TranslateController::t('Two')?></option>
                                            <option value="3"><?=TranslateController::t('Three')?></option>
                                            <option value="more"><?=TranslateController::t('More than Three')?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-submit">
                                    <?=Html::input('button','search',TranslateController::t('Search'),['class'=>'awe-btn awe-btn-lager awe-search'])?>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Search Hotel -->

                    <!-- Search Car -->
                    <div class="form-cn form-car tab-pane" id="form-car" role="tabpanel" >
                        <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                        <div class="form-search clearfix">
                            <div class="form-field field-destination">
                                <label for="destination"><span><?=TranslateController::t('Purpose:')?></span> <?=TranslateController::t('Country')?>, <?=TranslateController::t('Region')?>, <?=TranslateController::t('City')?></label>
                                <input type="text" id="destination" class="field-input">
                            </div>
                            <div class="form-field field-droping">
                                <input type="text" class="field-input" placeholder="<?=TranslateController::t('Droping off')?>">
                            </div>
                            <div class="form-field field-date">
                                <input type="text" class="field-input calendar-input" placeholder="<?=TranslateController::t('Start date')?>">
                            </div>
                            <div class="form-field field-date">
                                <input type="text" class="field-input calendar-input" placeholder="<?=TranslateController::t('End date')?>">
                            </div>
                            <div class="form-submit">
                                <button type="submit" class="awe-btn awe-btn-lager awe-search"><?=TranslateController::t('Search')?></button>
                            </div>
                        </div>
                    </div>
                    <!-- End Search Car -->

                    <!-- Search Flight-->
                    <div class="form-cn form-flight tab-pane" id="form-flight" role="tabpanel">
                        <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                        <div class="form-search clearfix">
                            <div class="form-field field-from">
                                <label for="flight-from"><span>Вилет из:</span> аеропорт</label>
                                <input type="text" name="flightfrom" id="flight-from" class="field-input">
                            </div>
                            <div class="form-field field-to">
                                <label for="flight-to"><span>В :</span> странна, аеропорт</label>
                                <input type="text" id="flight-to" class="field-input">
                            </div>
                            <div class="form-field field-date">
                                <input type="text" class="field-input calendar-input" placeholder="Departing">
                            </div>
                            <div class="form-field field-date">
                                <input type="text" class="field-input calendar-input" placeholder="Returning">
                            </div>
                            <div class="form-field field-select field-adult">
                                <div class="select">
                                    <span>Взрослые</span>
                                    <select>
                                        <option>один</option>
                                        <option>два</option>
                                        <option>три</option>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-field field-select field-children">
                                <div class="select">
                                    <span>Дети</span>
                                    <select>
                                        <option>один</option>
                                        <option>два</option>
                                        <option>три</option>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-submit">
                                <button type="submit" class="awe-btn awe-btn-medium awe-search">Поиск</button>
                            </div>
                        </div>
                    </div>
                    <!-- End Search Flight -->

                    <!-- Search Package -->
                    <div class="form-cn form-package tab-pane" id="form-package" role="tabpanel">
                        <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                        <ul class="form-radio">
                            <li>
                                <div class="radio-checkbox">
                                    <input type="radio" name="radio-1" id="radio-1" class="radio">
                                    <label for="radio-1"><?=TranslateController::t('Flight')?> + <?=TranslateController::t('Hotel')?></label>
                                </div>
                            </li>
                            <li>
                                <div class="radio-checkbox">
                                    <input type="radio" name="radio-1" id="radio-2" class="radio">
                                    <label for="radio-2"><?=TranslateController::t('Flight')?> + <?=TranslateController::t('Hotel')?> + <?=TranslateController::t('Bus')?></label>
                                </div>
                            </li>
                            <li>
                                <div class="radio-checkbox">
                                    <input type="radio" name="radio-1" id="radio-3" class="radio">
                                    <label for="radio-3"><?=TranslateController::t('Hotel')?> + <?=TranslateController::t('Bus')?></label>
                                </div>
                            </li>

                        </ul>
                        <div class="form-search clearfix">
                            <div class="form-field field-from">
                                <label for="filghtfrom"><span>Вылет из:</span> аэропорт...</label>
                                <input type="text" id="filghtfrom" class="field-input">
                            </div>
                            <div class="form-field field-to">
                                <label for="flightto"><span>В:</span> Странна, аэропорт</label>
                                <input type="text" id="flightto" class="field-input">
                            </div>
                            <div class="form-field field-date">
                                <input type="text" class="field-input calendar-input" placeholder="Departing">
                            </div>
                            <div class="form-field field-date">
                                <input type="text" class="field-input calendar-input" placeholder="Returning">
                            </div>
                            <div class="form-field field-select field-adults">
                                <div class="select">
                                    <span>Взрослые</span>
                                    <select>
                                        <option>один</option>
                                        <option>два</option>
                                        <option>три</option>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-field field-select field-children">
                                <div class="select">
                                    <span>Дети</span>
                                    <select>
                                        <option>один</option>
                                        <option>два</option>
                                        <option>три</option>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-submit">
                                <button type="submit" class="awe-btn awe-btn-medium awe-search">Поиск</button>
                            </div>
                        </div>
                    </div>
                    <!-- End Search Package -->

                    <!-- Search Tour-->
                    <div class="form-cn form-tour tab-pane" id="form-tour" role="tabpanel">
                        <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                        <div class="form-search clearfix">
                            <div class="form-field field-select field-region">
                                <div class="select">
                                    <span>Регион: <small>африка..</small></span>
                                    <select>
                                        <option>Африка</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-field field-select field-country">
                                <div class="select">
                                    <span>Странна</span>
                                    <select>
                                        <option>украина</option>
                                        <option>росия</option>
                                        <option>США</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-field field-select field-style">
                                <div class="select">
                                    <span>Тип тура</span>
                                    <select>

                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-submit">
                                <button type="submit" class="awe-btn awe-btn-medium awe-search">Поиск</button>
                            </div>
                        </div>
                    </div>
                    <!-- End Search Tour -->

                </div>
                <!-- End Tabs Content -->

            </div>
            <!-- End Banner Content -->

        </div>

    </section>
    <!--End Banner-->
    <?php
    echo Tabs::widget();
    ?>
    <!-- Sales -->
    <section class="sales">
        <!-- Title -->
        <div class="title-wrap">
            <div class="container">
                <div class="travel-title float-left">
                    <h2><?=TranslateController::t('Hot sale Today')?>: <span><?=$place?></span></h2>
                </div>
                <a href="#" title="" class="awe-btn awe-btn-5 awe-btn-lager arrow-right text-uppercase float-right"><?=TranslateController::t('SEE ALL')?></a>
            </div>
        </div>
        <!-- End Title -->
        <!-- Hot Sales Content -->
        <div class="container">
            <div class="sales-cn">
                <div class="row">
                    <!-- HostSales Item -->
                    <?php
                        $i=1;
                        foreach($hot as $h){
                            if($i===3||$i===4){
                                ?>
                                <div class="col-md-6">
                                    <div class="sales-item">
                                        <figure class="home-sales-img">
                                            <a href="<?=Url::to(['/site/tour-detail?id='.$h->id])?>" title="">
                                                <?php
                                                if(!empty($h->main_img)){
                                                    echo Html::img('/'.$h->main_img,['class'=>'lg_tur']);
                                                }
                                                else{
                                                    echo Html::img('/images/img-3.jpg');
                                                }
                                                ?>
                                            </a>
                                            <?php
                                            if($h->sale>0){
                                                ?>
                                                <figcaption>
                                                    Save <span><?=$h->sale?></span>%
                                                </figcaption>
                                                <?php
                                            }
                                            ?>
                                        </figure>
                                        <div class="home-sales-text">
                                            <div class="home-sales-name-places">
                                                <div class="home-sales-name">
                                                    <a href="<?=Url::to(['/site/tour-detail?id='.$h->id])?>" title=""><?=$h->name?></a>
                                                </div>
                                                <div class="home-sales-places">
                                                    <a href="" title=""><?=(!empty($c=$h->country->title))?$c:'';?></a>
                                                    <a href="" title=""><?=(!empty($c=$h->city->title))?', '.$c:'';?></a>
                                                </div>
                                            </div>
                                            <hr class="hr">
                                            <div class="price-box">
                                                <span class="price old-price"><?=TranslateController::t('From')?>  <del>$<?=$h->prise?></del></span>
                                                <span class="price special-price">$<?=$h->prise*(1-($h->sale/100))?><small>/<?=$h->price_type?></small></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            else{
                                ?>
                                <div class="col-xs-6 col-md-3">
                                    <div class="sales-item">
                                        <figure class="home-sales-img">
                                            <a href="<?=Url::to(['/site/tour-detail?id='.$h->id])?>" title="">
                                                <?php
                                                    if(!empty($h->main_img)){
                                                        echo Html::img('/'.$h->main_img,['class'=>'sm_tur']);
                                                    }
                                                    else{
                                                        echo Html::img('/images/img-1.jpg');
                                                    }
                                                ?>
                                            </a>
                                            <?php
                                                if($h->sale>0){
                                                    ?>
                                                    <figcaption>
                                                        Save <span><?=$h->sale?></span>%
                                                    </figcaption>
                                                    <?php
                                                }
                                            ?>
                                        </figure>
                                        <div class="home-sales-text">
                                            <div class="home-sales-name-places">
                                                <div class="home-sales-name">
                                                    <a href="<?=Url::to(['/site/tour-detail?id='.$h->id])?>" title=""><?=$h->name?></a>
                                                </div>
                                                <div class="home-sales-places">
                                                    <a href="" title=""><?=(!empty($c=$h->country->title))?$c:'';?></a>
                                                    <a href="" title=""><?=(!empty($c=$h->city->title))?', '.$c:'';?></a>
                                                </div>
                                            </div>
                                            <hr class="hr">
                                            <div class="price-box">
                                                <span class="price old-price">From  <del>$<?=$h->prise?></del></span>
                                                <span class="price special-price">$<?=$h->prise*(1-($h->sale/100))?><small>/<?=$h->price_type?></small></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            $i++;
                        }
                    ?>

                    <!-- End HostSales Item -->
                </div>
            </div>
        </div>
        <!-- End Hot Sales Content -->
    </section>
    <!-- End Sales -->

    <!-- Travel Destinations -->
    <section class="destinations">

        <!-- Title -->
        <div class="title-wrap">
            <div class="container">
                <div class="travel-title float-left">
                    <h2><?=TranslateController::t('Top destinations')?></h2>
                </div>
                <a href="#" title="" class="awe-btn awe-btn-5 arrow-right awe-btn-lager text-uppercase float-right"><?=TranslateController::t('SEE ALL')?></a>
            </div>
        </div>
        <!-- End Title -->

        <!-- Destinations Content -->
        <div class="destinations-cn">

            <!-- Background -->
            <div class="bg-parallax bg-2"></div>
            <!-- End Background -->

            <div class="container">
                <div class="row">
                    <!-- Destinations Filter -->
                    <div class="col-md-4 col-lg-3">
                        <div class="intro-filter">
                            <ul class="filter">
                                <li class="active">
                                    <a data-toggle="tab" href="#destinations-1"><i class="fa fa-map-marker"></i><?=TranslateController::t('Recommended for you')?></a>
                                </li>
                                <?php
                                    foreach($continent as $con){
                                        ?>
                                        <li>
                                            <a data-toggle="tab" href="#<?=str_replace(' ','',$con->name)?>"><i class="fa fa-map-marker"></i><?=$con->name?></a>
                                        </li>
                                        <?php
                                    }
                                ?>

                            </ul>
                        </div>

                    </div>
                    <!-- End Destinations Filter -->
                    <!-- Destinations Grid -->
                    <div class="col-md-8 col-lg-9">
                        <div class="tab-content destinations-grid">
                            <!-- Tab One -->
                            <div id="destinations-1" class="clearfix tab-pane fade active in ">
                                <!-- Destinations Item -->
                                <?php
                                    foreach($recomends as $rec){
                                        ?>
                                        <div class="col-xs-6 col-sm-4 col-md-6 col-lg-4">
                                            <div class="destinations-item ">
                                                <div class="destinations-text">
                                                    <div class="destinations-name">
                                                        <a href="" title=""><?=(!empty($rec->city->title))?$rec->city->title:$rec->country->title?></a>
                                                    </div>
                                                <span class="properties-nb">
                                                    <ins><?=$rec->visit?></ins> <?=TranslateController::t('Properties')?>
                                                </span>
                                                </div>
                                                <figure class="destinations-img">
                                                    <a href="" title="">
                                                        <?=(!empty($url=$rec->main_img))?Html::img('/'.$url):Html::img('/images/mini-1.jpg')?>
                                                    </a>
                                                </figure>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                ?>
                                <!-- End Destinations Item -->
                            </div>
                            <!-- End Tab One -->
                            <!-- Tab Two -->
                            <?php
                                foreach($grup as $key=>$value)
                                {
                                    ?>
                                    <div id="<?=$key?>" class="clearfix tab-pane fade">
                                        <!-- Destinations Item -->
                                        <?php
                                            foreach($value as $val){
                                                ?>
                                                <div class="col-xs-6 col-sm-4 col-md-6 col-lg-4">
                                                    <div class="destinations-item ">
                                                        <div class="destinations-text">
                                                            <div class="destinations-name">
                                                                <a href="" title=""><?=(!empty($val->city->title))?$val->city->title:$val->country->title?></a>
                                                            </div>
                                                <span class="properties-nb">
                                                    <ins><?=$val->visit?></ins> <?=TranslateController::t('Properties')?>
                                                </span>
                                                        </div>
                                                        <figure class="destinations-img">
                                                            <a href="" title="">
                                                                <?=(!empty($url=$val->main_img))?Html::img('/'.$url):Html::img('/images/mini-1.jpg')?>
                                                            </a>
                                                        </figure>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        ?>
                                        <!-- End Destinations Item -->
                                    </div>
                                    <?php
                                }
                            ?>
                            <!-- End Tab Two -->
                        </div>
                    </div>
                    <!-- ENd Destinations Grid -->
                </div>
            </div>
        </div>
        <!-- End Destinations Content -->
    </section>
    <!-- End Travel Destinations -->

    <!-- Travel Magazine -->
    <?php if(!empty($reviews_tour)){
        ?>
        <section class="magazine">
            <!-- Title -->
            <div class="title-wrap">
                <div class="container">
                    <div class="travel-title float-left">
                        <h2><?=TranslateController::t('Travel Journal')?></h2>
                    </div>
                    <a href="#" title="" class="awe-btn awe-btn-5 arrow-right awe-btn-lager text-uppercase float-right"><?=TranslateController::t('SEE ALL')?></a>
                </div>
            </div>
            <!-- End Title -->
            <!-- Magazine Content -->
            <div class="container">
                <div class="magazine-cn">
                    <div class="row">
                        <!-- Magazine Descript -->
                        <div class="col-lg-6">
                            <div class="magazine-ds">
                                <div id="owl-magazine-ds">
                                    <!-- Magazine Descript Item -->
                                    <?php
                                    foreach ($reviews_tour as $review) {
                                        if($review->type_review===1){
                                            ?>
                                            <div class="magazine-item">
                                                <div class="magazine-header">
                                                    <h2><?=$review->name?></h2>
                                                    <ul>
                                                        <li>by <a href="" title=""><?=$review->user->username?></a></li>
                                                        <li><?=$review->update_at?></li>
                                                    </ul>
                                                    <hr class="hr">
                                                </div>
                                                <div class="magazine-body">
                                                    <?=$review->description?>
                                                </div>
                                                <div class="magazine-footer clearfix">
                                                    <div class="post-share magazine-share float-left">
                                                        <a href="" title=""><i class="fa fa-facebook"></i></a>
                                                        <a href="" title=""><i class="fa fa-twitter"></i></a>
                                                        <a href="" title=""><i class="fa fa-google-plus"></i></a>
                                                    </div>
                                                    <a href="" title="" class="awe-btn awe-btn-5 arrow-right awe-btn-lager text-uppercase float-right"><?=TranslateController::t('SEE ALL')?></a>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }

                                    ?>

                                    <!-- End Magazine Descript Item -->
                                </div>
                            </div>
                        </div>
                        <!-- End Magazine Descript -->
                        <!-- Magazine Thumnail -->
                        <div class="col-lg-6">
                            <div class="magazine-thum" id="magazine-thum">
                                <!--Thumnail Item-->
                                <?php
                                foreach ($reviews_tour as $review) {
                                    if($review->type_review){
                                        ?>
                                        <div class="thumnail-item active clearfix">
                                            <figure class="float-left">
                                                <?=(!empty($url = $review->tur->main_img))?Html::img('/'.$url):Html::img('/images/picture292.png')?>
                                            </figure>
                                            <div class="thumnail-text">
                                                <h4><?=$review->tur->country->title.' '.$review->tur->city->title.' '. $review->transport_type .', '.$review->tur->name?></h4>
                                                <span><?=$review->update_at?></span>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }

                                ?>
                                <!--End Thumnail Item-->
                            </div>
                        </div>
                        <!-- End Magazine Thumnail -->
                    </div>
                </div>
            </div>
            <!-- End Magazine Content -->
        </section>
        <?php
    }?>

    <!-- End Travel Magazine -->

    <!-- Confidence and Subscribe  -->
    <section class="confidence-subscribe">
        <!-- Background -->
        <div class="bg-parallax bg-3"></div>
        <!-- End Background -->
        <div class="container">
            <div class="row cs-sb-cn">

                <!-- Confidence -->
                <div class="col-md-6">
                    <div class="confidence">
                        <h3><?=TranslateController::t('Book of Complaints')?></h3>
                        <ul>
                            <?php
                            foreach ($reviews_tour as $review) {
                                if($review->type_review===0){
                                    ?>
                                        <li>
                                            <span class="label-nb"><?=$review->id?></span>
                                            <h5><?=$review->name?></h5>
                                            <p><?=$review->description?></p>
                                        </li>
                                    <?php
                                }
                            }

                            ?>

                        </ul>
                    </div>
                </div>
                <!-- End Confidence -->
                <!-- Subscribe -->
                <div class="col-md-6">
                    <div class="subscribe">
                        <h3>Подписывайтесь на нашу новостную рассылку</h3>
                        <p>Введите свой адрес электронной почты, и мы вышлем Вам наши регулярные рекламные сообщения, упакованные со специальными предложениями, выгодными предложениями и огромными скидками</p>
                        <!-- Subscribe Form -->
                        <div class="subscribe-form">
                            <form action="#" method="get">
                                <input type="text" name="" value="" placeholder="Your email" class="subscribe-input">
                                <button type="submit" class="awe-btn awe-btn-5 arrow-right text-uppercase awe-btn-lager">подписатся</button>
                            </form>
                        </div>
                        <!-- End Subscribe Form -->
                        <!-- Follow us -->
                        <div class="follow-us">
                            <h4>Подписывайтесь на нас</h4>
                            <div class="follow-group">
                                <a href="" title=""><i class="fa fa-facebook"></i></a>
                                <a href="" title=""><i class="fa fa-twitter"></i></a>
                                <a href="" title=""><i class="fa fa-pinterest"></i></a>
                                <a href="" title=""><i class="fa fa-linkedin"></i></a>
                                <a href="" title=""><i class="fa fa-instagram"></i></a>
                                <a href="" title=""><i class="fa fa-google-plus"></i></a>
                                <a href="" title=""><i class="fa fa-digg"></i></a>
                            </div>
                        </div>
                        <!-- Follow us -->
                    </div>
                </div>
                <!-- End Subscribe -->

            </div>
        </div>
    </section>
    <!-- End Confidence and Subscribe  -->


</div>