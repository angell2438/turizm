<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 17.05.2016
 * Time: 0:46
 */
use yii\helpers\Html;
?>
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->
    <!-- Logo -->
    <div class="logo-banner text-center">
        <a href="" title="">
            <?=Html::img('/images/logo-banner.png')?>
        </a>
    </div>
    <!-- Logo -->
</section>
<!--End Banner-->

<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn bg-white clearfix">
            <section class="cart-cn">
                <h1>Your cart</h1>

                <div class="table-responsive">
                    <table class="table tb-cart">
                        <thead>
                        <tr>
                            <th>Booking</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Number</th>
                            <th class="text-center">Total</th>
                        </tr>
                        </thead>
                        <tr class="border">
                            <td>
                                <div class="cart-room">
                                    <h2>Grand Plaza Serviced Apartments</h2>
                                            <span class="star-room">
                                                <i class="glyphicon glyphicon-star"></i>
                                                <i class="glyphicon glyphicon-star"></i>
                                                <i class="glyphicon glyphicon-star"></i>
                                                <i class="glyphicon glyphicon-star"></i>
                                                <i class="glyphicon glyphicon-star"></i>
                                            </span>
                                    <ul>
                                        <li>
                                            <span>Check-in:</span>
                                            Thu 30 Oct, 2014
                                        </li>
                                        <li>
                                            <span>Check-out:</span>
                                            Sat 01 Nov, 2014
                                        </li>
                                        <li>
                                            <span>Stay:</span>
                                            2 Nights, 1 Room, Max 2 Adult(s)
                                        </li>
                                        <li>
                                            <span>Room:</span>
                                            Luxury View Suite
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <div class="group-date">
                                    <span class="date">Fri, Apr 10<small>to</small>Fri, Apr 17</span>
                                    <span class="date">Fri, Apr 10<small>to</small>Fri, Apr 17</span>
                                </div>
                            </td>
                            <td>
                                <div class="group-price">
                                    <span class="price">$345<small>/night</small></span>
                                    <span class="price">$345<small>/night</small></span>
                                </div>
                            </td>
                            <td>
                                <div class="group-select">
                                    <div class="select">
                                        <span>1 room</span>
                                        <select name="room">
                                            <option value="1">1 room</option>
                                            <option value="2">2 room</option>
                                            <option value="3">3 room</option>
                                            <option value="4">4 room</option>
                                        </select>
                                    </div>

                                    <div class="select">
                                        <span>1 room</span>
                                        <select name="room">
                                            <option value="1">1 room</option>
                                            <option value="2">2 room</option>
                                            <option value="3">3 room</option>
                                            <option value="4">4 room</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="price sub-total-price">$345</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="cart-flight">

                                    <h2>Departure  to New York</h2>
                                    <div class="airline">
                                        <?=Html::img("/images/flight/icon/icon-1.png")?><span>American Airlines</span>
                                    </div>

                                    <div class="stop-cn">
                                        <label>1h15m</label>
                                                <span class="stops _2">
                                                    <span class="stop"></span>
                                                    <span class="stop"></span>
                                                </span>
                                        <label>2 Stops</label>
                                    </div>
                                    <div class="group-time">
                                        <p>08:35 <small>AM</small><span>BKK</span></p>
                                        <p>08:35 <small>AM</small><span>BKK</span></p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="date">Fri, Apr 10</span>
                            </td>
                            <td>
                                <span class="price">$215</span>
                            </td>
                            <td>
                                <div class="select">
                                    <span>1 person</span>
                                    <select name="room">
                                        <option value="1">1 person</option>
                                        <option value="2">2 person</option>
                                        <option value="3">3 person</option>
                                        <option value="4">4 person</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <span class="price sub-total-price">$300</span>
                            </td>
                        </tr>
                        <tr class="border">
                            <td>
                                <div class="cart-flight">

                                    <h2>Return to Hanoi</h2>
                                    <div class="airline">
                                        <?=Html::img("/images/flight/icon/icon-1.png")?><span>American Airlines</span>
                                    </div>

                                    <div class="stop-cn">
                                        <label>1h15m</label>
                                                <span class="stops _2">
                                                    <span class="stop"></span>
                                                    <span class="stop"></span>
                                                </span>
                                        <label>2 Stops</label>
                                    </div>
                                    <div class="group-time">
                                        <p>08:35 <small>AM</small><span>BKK</span></p>
                                        <p>08:35 <small>AM</small><span>BKK</span></p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="date">Fri, Apr 10</span>
                            </td>
                            <td>
                                <span class="price">$215</span>
                            </td>
                            <td>
                                <div class="select">
                                    <span>1 person</span>
                                    <select name="room">
                                        <option value="1">1 person</option>
                                        <option value="2">2 room</option>
                                        <option value="3">3 person</option>
                                        <option value="4">4 person</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="border">
                            <td>
                                <div class="cart-car">
                                    <div class="car-img">
                                        <?=Html::img("/images/car/img-7.jpg")?>
                                    </div>
                                    <h2>Economy 2/4Door Car</h2>
                                    <p>Chevy Aveo or similar</p>
                                    <ul>
                                        <li><span>Picking up:</span> New York, NY (JFK)</li>
                                        <li><span>Dropping off:</span> Los Angeles, CA (LAX)</li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <span class="date">Fri, Apr 10<small>to</small>Fri, Apr 17</span>
                            </td>
                            <td>
                                <span class="price">$215</span>
                            </td>
                            <td>
                                <div class="select">
                                    <span>1 car</span>
                                    <select name="room">
                                        <option value="1">1 car</option>
                                        <option value="2">2 car</option>
                                        <option value="3">3 car</option>
                                        <option value="4">4 car</option>
                                    </select>
                                </div>
                            </td>
                            <td><span class="price sub-total-price">$300</span></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="cart-flight">

                                    <h2>Tokyo to New York</h2>
                                    <div class="airline">
                                        <?=Html::img("/images/flight/icon/icon-1.png")?><span>American Airlines</span>
                                    </div>

                                    <div class="stop-cn">
                                        <label>13h 52m</label>
                                                <span class="stops">
                                                    <span class="stop"></span>
                                                </span>
                                        <label>1 Stop</label>
                                    </div>
                                    <div class="group-time">
                                        <p>08:35 <small>AM</small><span>BKK</span></p>
                                        <p>08:35 <small>AM</small><span>BKK</span></p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="date">Fri, Apr 10</span>
                            </td>
                            <td>
                                <span class="price">$215</span>
                            </td>
                            <td>
                                <div class="select">
                                    <span>1 person</span>
                                    <select name="room">
                                        <option value="1">1 person</option>
                                        <option value="2">2 person</option>
                                        <option value="3">3 person</option>
                                        <option value="4">4 person</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <span class="price sub-total-price">$300</span>
                            </td>
                        </tr>
                        <tr class="border">
                            <td>
                                <div class="cart-room">
                                    <h2>Grand Plaza Serviced Apartments</h2>
                                            <span class="star-room">
                                                <i class="glyphicon glyphicon-star"></i>
                                                <i class="glyphicon glyphicon-star"></i>
                                                <i class="glyphicon glyphicon-star"></i>
                                                <i class="glyphicon glyphicon-star"></i>
                                                <i class="glyphicon glyphicon-star"></i>
                                            </span>
                                    <ul>
                                        <li>
                                            <span>Check-in:</span>
                                            Thu 30 Oct, 2014
                                        </li>
                                        <li>
                                            <span>Check-out:</span>
                                            Sat 01 Nov, 2014
                                        </li>
                                        <li>
                                            <span>Stay:</span>
                                            2 Nights, 1 Room, Max 2 Adult(s)
                                        </li>
                                        <li>
                                            <span>Room:</span>
                                            Luxury View Suite
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <span class="date">Fri, Apr 10<small>to</small>Fri, Apr 17</span>
                            </td>
                            <td>
                                <span class="price">$345<small>/night</small></span>
                            </td>
                            <td>
                                <div class="select">
                                    <span>1 person</span>
                                    <select name="room">
                                        <option value="1">1 person</option>
                                        <option value="2">2 person</option>
                                        <option value="3">3 person</option>
                                        <option value="4">4 person</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <span class="price sub-total-price">$345</span>
                            </td>
                        </tr>
                        <tr class="border">
                            <td>
                                <div class="cart-cruise">
                                    <h2>7-night Caribbean Cruise</h2>
                                    <ul>
                                        <li><span>Ship:</span> Carnival Glory</li>
                                        <li><span>Cabin Type:</span> Guarantee - Oceanview</li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <span class="date">Fri, Apr 10<small>to</small>Fri, Apr 17</span>
                            </td>

                            <td>
                                <span class="price">$345</span>
                            </td>
                            <td>
                                <div class="select">
                                    <span>1 person</span>
                                    <select name="room">
                                        <option value="1">1 person</option>
                                        <option value="2">2 person</option>
                                        <option value="3">3 person</option>
                                        <option value="4">4 person</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <span class="price sub-total-price">$345</span>
                            </td>
                        </tr>
                        <tr class="border">
                            <td>
                                <div class="cart-cruise">
                                    <h2>Berlin &amp; Prague plus Vienna</h2>
                                    <ul>
                                        <li>7 days, 6 nights</li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <span class="date">Fri, Apr 10<small>to</small>Fri, Apr 17</span>
                            </td>

                            <td>
                                <span class="price">$345</span>
                            </td>
                            <td>
                                <div class="select">
                                    <span>1 person</span>
                                    <select name="room">
                                        <option value="1">1 person</option>
                                        <option value="2">2 person</option>
                                        <option value="3">3 person</option>
                                        <option value="4">4 person</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <span class="price sub-total-price">$345</span>
                            </td>
                        </tr>
                        <tfoot>
                        <tr>
                            <td colspan="5">
                                <span class="price sub-total-price">$345</span>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>

                <p class="check-out-cart text-center">
                    <a href="#" class="awe-btn awe-btn-1 awe-btn-lager">Check Out</a>
                </p>
            </section>
        </div>
    </div>
</div>
