<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 15.05.2016
 * Time: 22:20
 */
use yii\helpers\Html;
use app\commands\TranslateController;
$this->title = \app\commands\TranslateController::t('Tour List');
$lang =\app\models\Lang::getCurrent();
$lang= $lang->url;
?>
<!--Banner-->
<section class="sub-banner">
        <!--Background-->
        <div class="bg-parallax bg-1"></div>
        <!--End Background-->
        <!-- Logo -->
        <div class="logo-banner text-center">
            <a href="" title="">
                <?=Html::img('/images/logo-banner.png')?>
            </a>
        </div>
        <!-- Logo -->
    </section>
    <!--End Banner-->

    <!-- Main -->
<div class="main">
        <div class="container">
            <div class="main-cn tour-page bg-white clearfix">
                <div class="row">

                    <!-- Cruise Right -->
                    <div class="col-md-9 col-md-push-3">

                        <section class="cruise-list">

                            <!-- Sort by and View by -->
                            <div class="sort-view clearfix">
                                <!-- Sort by -->

                                    <div class="sort-by float-left">
                                        <form class="search-form_tour" action="">
                                        <label>Sort by: </label>

                                        <div class="sort-select select float-left form-group">
                                            <input type="text" class="form-control" placeholder="<?=TranslateController::t('Start')?>" name="start">
                                        </div>

                                        <div class="sort-select select float-left form-group">
                                            <input type="text" class="form-control" placeholder="<?=TranslateController::t('End')?>" name="end">
                                        </div>

                                        <div class="sort-select select float-left form-group">
                                            <span>Length</span>
                                            <select name="length">
                                                <option value=""><?=TranslateController::t('Default')?></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="more"><?=TranslateController::t('More than three')?></option>
                                            </select>
                                        </div>

                                        <!--<div class="sort-select select float-left">
                                            <span>Cruise Line</span>
                                            <select>
                                                <option>Cruise Line</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>-->

                                        <div class="sort-select select float-left form-group">
                                            <span>Pricing</span>
                                            <select name="price">
                                                <option value=""><?=TranslateController::t('Default')?></option>
                                                <option value="SORT_DESC"><?=TranslateController::t('Descending')?></option>
                                                <option value="SORT_ASC"><?=TranslateController::t('Ascending')?></option>
                                            </select>
                                        </div>
                                        </form>

                                    </div>


                                <!-- End Sort by -->
                                <!-- View by -->

                                <!-- View by -->
                            </div>
                            <!-- End Sort by and View by -->

                            <!-- Cruise Content -->
                            <div class="cruise-list-cn tour-list-cn">
                                <?php
                                foreach ($models as $model) {
                                    ?>
                                    <div class="cruise-item">
                                        <figure class="cruise-img">
                                            <?=Html::a((!empty($url=$model->main_img))?Html::img('/'.$url):Html::img('/images/mini-1.jpg'),['/site/tour-detail?id='.$model->id])?>
                                        </figure>
                                        <div class="cruise-text">
                                            <div class="cruise-name">
                                                <?=Html::a($model->name,['/site/tour-detail?id='.$model->id])?>
                                            </div>
                                            <ul class="ship-port">
                                                <li>
                                                    <span class="label">Featuring:</span>
                                                    <?php
                                                    foreach ($model->dayForTurs as $day ) {
                                                        $str = ((!empty($day->city->title))?$day->city->title:$day->city->title).',';
                                                        }
                                                    echo trim($str,',');
                                                    ?>
                                                </li>
                                            </ul>
                                            <div class="price-box">
                                            <span class="price">
                                                <?php
                                                    if(!empty($model->prise)){
                                                        echo TranslateController::t('From');
                                                        ?><br>
                                                        <ins>$<?=$model->prise*(1-$model->sale/100)?></ins>
                                                        <?php
                                                    }
                                                ?>
                                            </span>
                                            <span class="price night">
                                                <ins><?=$model->long_time?></ins><small>/day</small>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }

                                ?>

                            </div>
                            <!-- End Cruise Content -->
                            <div class="page-navigation-cn">
                            <?= \yii\widgets\LinkPager::widget([
                                'pagination' => $pages,
                                'options'=>['class'=>'page-navigation'],
                                'firstPageLabel'=>'First',   // Set the label for the "first" page button
                                'lastPageLabel'=>'Last',    // Set the label for the "last" page button
                                'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
                                'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
                                'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed

                            ]);?>
                            </div>

                        </section>
                    </div>
                    <!-- End Cruise Right -->

                    <!-- Sidebar Hotel -->
                    <div class="col-md-3 col-md-pull-9">
                        <!-- Sidebar Content -->
                        <div class="sidebar-cn">
                            <!-- Search Result -->
                            <div class="search-result">
                                <p>
                                    <?=TranslateController::t('We found')?> <br>
                                    <ins><?=$count?></ins> <span><?=TranslateController::t('properties availability')?></span>
                                </p>
                            </div>
                            <!-- End Search Result -->
                            <!-- Search Form Sidebar -->

                            <div class="search-sidebar">
                                <div class="row">
                                    <div class="form-search clearfix">
                                        <form class="search-form_tour" action="">
                                            <div class="form-field col-md-12">
                                                <label for="region"><span><?=TranslateController::t('Region')?>:</span><?=TranslateController::t('Region')?>, <?=TranslateController::t('City')?></label>
                                                <input type="text" name="region" id="region" value="" class="field-input">
                                            </div>
                                            <div class="form-field field-select col-md-12">
                                                <div class="select">
                                                    <span>Country</span>
                                                    <select name="guest">
                                                        <option value="">Country</option>
                                                        <?php
                                                        foreach ($country as $key=>$item) {
                                                            ?>
                                                            <option value="<?=$key?>"><?=$item?></option>
                                                            <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-submit col-md-12">
                                                <input type="button" class="awe-btn awe-btn-medium awe-search tur-search" value="Search">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End Search Form Sidebar -->
                            <!-- Narrow your results -->
                            <!--<div class="narrow-results">
                                <h6>Narrow your results</h6>
                                <div class="narrow-form">
                                    <form action="action" method="get">
                                        <input type="text" name="" class="narrow-input" placeholder="Property name contains:">
                                        <button class="submit-narrow"></button>
                                    </form>
                                </div>
                            </div>-->
                            <!-- End Narrow your results -->
                            <!-- Price Slider -->
                            <div class="widget-sidebar price-slider-sidebar">
                                <h4 class="title-sidebar">Price</h4>
                                <form class="search-form_tour" action="">
                                    <div class="slider-sidebar price-slider" id="price-slider">
                                        <input type="text" name="price_range" class="range" value="0,1500" />
                                    </div>
                                </form>
                            </div>
                            <!-- End Price Slider -->
                            <!-- Hotel Location -->
                            <div class="widget-sidebar facilities-sidebar location">
                                <h4 class="title-sidebar"><?=TranslateController::t('Location')?></h4>

                                <form class="search-form_tour" action="">
                                    <ul class="widget-ul">
                                        <?php
                                        foreach ($country as $key=>$value) {
                                            ?>
                                            <li>
                                                <div class="radio-checkbox">
                                                    <input id="<?=$key?>" name="Check[<?=$key?>]" type="checkbox" class="checkbox" />
                                                    <label for="<?=$key?>"><?=$value?></label>
                                                </div>
                                            </li>
                                            <?php
                                        }

                                        ?>

                                    </ul>
                                </form>
                            </div>
                            <!-- End Hotel facilities -->

                        </div>
                        <!-- End Sidebar Content -->
                    </div>
                    <!-- End Sidebar Hotel -->

                </div>
            </div>
        </div>
    </div>
<script>
    $('.sort-select.select input').datepicker({
        language: "<?=$lang?>",
        format: "mm/d/yyyy"
    });
</script>
    <!-- End Main -->


