<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 16.05.2016
 * Time: 20:34
 */
use app\commands\TranslateController;
use yii\helpers\Html;
foreach ($models as $model) {
    ?>
    <div class="cruise-item">
        <figure class="cruise-img">
            <?=Html::a((!empty($url=$model->main_img))?Html::img('/'.$url):Html::img('/images/mini-1.jpg'),['/site/tour-detail?id='.$model->id])?>
        </figure>
        <div class="cruise-text">
            <div class="cruise-name">
                <?=Html::a($model->name,['/site/tour-detail?id='.$model->id])?>
            </div>
            <ul class="ship-port">
                <li>
                    <span class="label">Featuring:</span>
                    <?php
                    foreach ($model->dayForTurs as $day ) {
                        $str = ((!empty($day->city->title))?$day->city->title:$day->city->title).',';
                    }
                    echo trim($str,',');
                    ?>
                </li>
            </ul>
            <div class="price-box">
                  <span class="price">
                   <?php
                   if(!empty($model->prise)){
                       echo TranslateController::t('From');
                       ?><br>
                       <ins>$<?=$model->prise*(1-$model->sale/100)?></ins>
                       <?php
                   }
                   ?>
                    </span>
                    <span class="price night">
                      <ins><?=$model->long_time?></ins><small>/day</small>
                    </span>
            </div>
        </div>
    </div>
    <?php
}

?>