<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 17.05.2016
 * Time: 1:03
 */
use yii\helpers\Html;
?>

<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->
    <!-- Logo -->
    <div class="logo-banner text-center">
        <a href="" title="">
            <?=Html::img("/images/logo-banner.png")?>
        </a>
    </div>
    <!-- Logo -->
</section>
<!--End Banner-->

<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn bg-white clearfix">
            <div class="step">
                <!-- Step -->
                <ul class="payment-step text-center clearfix">
                    <li class="step-select">
                        <span>1</span>
                        <p>Выберите Ваш номер</p>
                    </li>
                    <li class="step-part">
                        <span>2</span>
                        <p>Ваш заказ и платежные реквизиты</p>
                    </li>
                    <li>
                        <span>3</span>
                        <p>Бронирование Завершено!</p>
                    </li>
                </ul>
                <!-- ENd Step -->
            </div>
            <!-- Payment Room -->
            <div class="payment-room">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="payment-info">
                            <h2>Grand Plaza Serviced Apartments </h2>
                                    <span class="star-room">
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                    </span>
                            <ul>
                                <li>
                                    <span>Расположение::</span>
                                    42 Princes Square, London W2 4AD
                                </li>
                                <li>
                                    <span>Заезд:</span>
                                    Thu 30 Oct, 2014
                                </li>
                                <li>
                                    <span>Выезд:</span>
                                    Sat 01 Nov, 2014
                                </li>
                                <li>
                                    <span>Пребывание:</span>
                                    2 Nights, 1 Room, Max 2 Adult(s)
                                </li>
                                <li>
                                    <span>Номер:</span>
                                    Luxury View Suite
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="payment-price">

                            <figure>
                                <?=Html::img("/images/hotel/img-9.jpg")?>
                            </figure>
                            <div class="total-trip">
                                        <span>
                                            1 Room x 2 Nights<br>
                                            $501.33<small>/night</small>
                                        </span>

                                <p>
                                    Trip Total: <ins>$365</ins>

                                    <i>Hotel tax 7% not included, Service charge 10% not included
                                    </i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Payment Room -->

            <div class="payment-form">
                <div class="row form">
                    <div class="col-md-6">
                        <h2>KЛичная информацыя</h2>
                        <div class="form-field">
                            <input type="text" placeholder="Имя" class="field-input">
                        </div>
                        <div class="form-field">
                            <input type="text" placeholder="Фамилия" class="field-input">
                        </div>
                        <div class="form-field">
                            <input type="text" placeholder="Email" class="field-input">
                        </div>
                        <!--
                                                        <div class="form-field">
                                                            <input type="text" placeholder="Email (confirm)" class="field-input">
                                                        </div>
                        -->
                        <div class="form-field">
                            <input type="text" placeholder="номер" class="field-input">
                        </div>
                        <!--
                                                        <div class="form-field">
                                                            <input type="text" placeholder="Country of Passport" class="field-input">
                                                        </div>
                        -->
                        <div class="radio-checkbox">
                            <input type="checkbox" class="checkbox" id="accept">
                            <label for="accept">Я не проживающих в гостинице. Я делаю это номер для кого-то еще.</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h2>Платежные реквизиты</h2>
                        <span>Выберите метод оплаты  <?=Html::img("/images/icon-payment.png")?></span>
                        <ul>
                            <li>
                                <div class="radio-checkbox">
                                    <input type="radio" name="radio-1" id="radio-1" class="radio">
                                    <label for="radio-1">Visa</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio-checkbox">
                                    <input type="radio" name="radio-1" id="radio-2" class="radio">
                                    <label for="radio-2">MasterCard</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio-checkbox">
                                    <input type="radio" name="radio-1" id="radio-3" class="radio">
                                    <label for="radio-3">JCB</label>
                                </div>
                            </li>
                            <!--
                                                                <li>
                                                                    <div class="radio-checkbox">
                                                                        <input type="radio" name="radio-1" id="radio-4" class="radio">
                                                                        <label for="radio-4">American Express</label>
                                                                    </div>
                                                                </li>
                            -->
                            <li>
                                <div class="radio-checkbox">
                                    <input type="radio" name="radio-1" id="radio-5" class="radio">
                                    <label for="radio-5">PayPal</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio-checkbox">
                                    <input type="radio" name="radio-1" id="radio-6" class="radio">
                                    <label for="radio-6">Carte Bleue</label>
                                </div>
                            </li>

                        </ul>
                        <div class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6 cart-number">

                                <label>Номер карты</label>

                                <div class="row">

                                    <div class="col-xs-3">
                                        <div class="form-field">
                                            <input type="text" class="field-input">
                                        </div>
                                    </div>

                                    <div class="col-xs-3">
                                        <div class="form-field">
                                            <input type="text" class="field-input">
                                        </div>
                                    </div>

                                    <div class="col-xs-3">
                                        <div class="form-field">
                                            <input type="text" class="field-input">
                                        </div>
                                    </div>

                                    <div class="col-xs-3">
                                        <div class="form-field">
                                            <input type="text" class="field-input">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6 card-holder">
                                <label>Имя владельца карты</label>
                                <div class="form-field">
                                    <input type="text" class="field-input">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6 expiry-date">
                                <label>Срок годности</label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-field">
                                            <input type="text" class="field-input calendar-input">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-field">
                                            <input type="text" class="field-input calendar-input">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6 cvc-code">
                                <label>CVC-code</label>
                                <div class="form-field">
                                    <input type="text" class="field-input">
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="submit text-center">
                    <p>
                        By selecting to complete this booking I acknowledge that I have read and accept the <span>rules &amp; restrictions terms &amp; conditions</span> , and <span>privacy policy</span>.
                    </p>

                    <button class="awe-btn awe-btn-1 awe-btn-lager">Забронировать</button>

                </div>

            </div>
        </div>
    </div>
</div>
