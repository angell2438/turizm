<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 15.05.2016
 * Time: 23:30
 */
use yii\helpers\Html;
use app\commands\TranslateController;
$this->title = \app\commands\TranslateController::t('Tour Detail');
?>
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->
    <!-- Logo -->
    <div class="logo-banner text-center">
        <a href="" title="">
            <?=Html::img('/images/logo-banner.png')?>
        </a>
    </div>
    <!-- Logo -->
</section>

<div class="main main-dt">
    <div class="container">
        <div class="main-cn detail-page bg-white clearfix">


            <!-- Header Detail -->
            <section class="head-detail">
                <div class="head-dt-cn">
                    <div class="row">
                        <div class="col-sm-7">
                            <h1><?=$model->long_time?> Days, <?=$model->name?></h1>
                        </div>
                        <div class="col-sm-5 text-right">
                            <p class="price-book">
                                <?=TranslateController::t('From')?>-<span>$<?=$model->prise*(1-$model->sale/100)?></span>/<?=$model->price_type?>
                                <a href="" title="" class="awe-btn awe-btn-1 awe-btn-lager">Book Now</a>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header Detail -->

            <!-- Detail Slide -->
            <section class="detail-slider">
                <!-- Lager Image -->
                <div class="slide-room-lg">
                    <div id="slide-room-lg">
                        <?php
                        if(!empty($model->main_img)){
                            echo Html::img('/'.$model->main_img);
                        }
                        $img = explode(';',$model->img);
                        foreach ($img as $item) {
                            if(!empty($item)){
                                echo Html::img('/'.$item);
                            }
                        }
                        ?>
                    </div>
                </div>
                <!-- End Lager Image -->
                <!-- Thumnail Image -->
                <div class="slide-room-sm">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div id="slide-room-sm">
                                <?php
                                if(!empty($model->main_img)){
                                    echo Html::img('/'.$model->main_img);
                                }
                                $img = explode(';',$model->img);
                                foreach ($img as $item) {
                                    if(!empty($item)){
                                        echo Html::img('/'.$item);
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Thumnail Image -->
            </section>
            <!-- End Detail Slide -->

            <!-- Tour Overview -->
            <section class="tour-overview detail-cn" id="tour-overview">
                <div class="row">
                    <div class="col-lg-3 detail-sidebar">
                        <div class="scroll-heading">
                            <h2><?=TranslateController::t('Overview')?></h2>
                            <hr class="hr">
                            <a href="#optional-acitivites" title=""><?=TranslateController::t('Optional activities')?></a>
                            <a href="#accomodation" title=""><?=TranslateController::t('Accomodation')?></a>
                            <a href="#tour-meals" title=""><?=TranslateController::t('Meals')?></a>
                            <a href="#tour-necessary"><?=TranslateController::t('No Tip Necessary')?></a>
                        </div>
                    </div>

                    <!-- Tour Overview Content -->
                    <div class="col-lg-9 tour-overview-cn">

                        <!-- Description -->
                        <div class="tour-description">
                            <h2 class="title-detail">
                                <?=TranslateController::t('Description')?>
                            </h2>
                            <div class="tour-detail-text">
                                <p>
                                    <?=$model->description?>
                                </p>
                            </div>
                        </div>
                        <!-- End Description -->

                        <div class="tour-itinerary">
                            <h2 class="title-detail"><?=TranslateController::t('Itinerary')?></h2>
                            <!-- Accordion -->
                            <div class="panel-group no-margin" id="accordion">

                                <!-- Accordion 1 -->
                                <?php foreach ($day_tur as $item) {
                                    ?>
                                    <div class="panel">
                                        <div class="panel-heading" id="headingOne">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                    <small><?=TranslateController::t('Day')?> <?=$item->number_day?>:</small><?=$item->name?>
                                                    <span class="icon fa fa-angle-down"></span>
                                                </a>
                                            </h4>

                                        </div>
                                        <div  id="collapseOne" class="panel-collapse collapse in" aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <?=$item->description?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }?>

                                <!-- End Accordion 1 -->


                            </div>
                            <!-- Accordion -->
                        </div>
                    </div>
                    <!-- End Tour Overview Content -->

                </div>
            </section>
            <!-- End Tour Overview -->

            <!-- Optional Activities -->
            <section class="optional-acitivites detail-cn" id="optional-acitivites">
                <div class="row">
                    <div class="col-lg-3 detail-sidebar">
                        <div class="scroll-heading">
                            <h2><?=TranslateController::t('Optional activities')?></h2>
                            <hr class="hr">
                            <a href="#tour-overview" title=""><?=TranslateController::t('Overview')?></a>
                            <a href="#accomodation" title=""><?=TranslateController::t('Accomodation')?></a>
                            <a href="#tour-meals" title=""><?=TranslateController::t('Meals')?></a>
                            <a href="#tour-necessary"><?=TranslateController::t('No Tip Necessary')?></a>
                        </div>
                    </div>
                    <div class="col-lg-9 optional-acitivites-cn">
                        <!-- Optional Text -->
                        <div class="tour-detail-text">
                            <p>
                                <?=$model->optional_activities?>
                            </p>
                        </div>
                        <!-- End Optional Text -->
                        <div class="optional-list">
                            <!-- Optional Item -->
                            <?php foreach ($responses as $key=>$value) {
                                ?>
                                <h4><?=$key?></h4>
                                <?php
                                foreach ($value as $key=>$item) {
                                    ?>
                                    <p>
                                        <span><?=$key?>:</span> $<?=$item['price']?>/<?=$item['price_type']?> <?=$item['description']?>
                                    </p>
                                    <?php
                                }
                                ?>
                                <?php
                            }?>

                            <!-- End Optional Item -->

                        </div>
                    </div>
                </div>
            </section>
            <!-- End Optional Activities -->

            <!-- Accomodation -->
            <section class="accomodation detail-cn" id="accomodation">
                <div class="row">
                    <div class="col-lg-3 detail-sidebar">
                        <div class="scroll-heading">
                            <h2><?=TranslateController::t('Accomodation')?></h2>
                            <hr class="hr">
                            <a href="#tour-overview" title=""><?=TranslateController::t('Overview')?></a>
                            <a href="#optional-acitivites" title=""><?=TranslateController::t('Optional activities')?></a>
                            <a href="#tour-meals" title=""><?=TranslateController::t('Meals')?></a>
                            <a href="#tour-necessary"><?=TranslateController::t('No Tip Necessary')?></a>
                        </div>
                    </div>
                    <div class="col-lg-9 accomodation-cn">
                        <div class="tour-detail-text">
                            <?=$model->accommodation?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Accomodation-->

            <!-- Meals -->
            <section class="tour-meals detail-cn" id="tour-meals">
                <div class="row">
                    <div class="col-lg-3 detail-sidebar">
                        <div class="scroll-heading">
                            <h2><?=TranslateController::t('Meals')?></h2>
                            <hr class="hr">
                            <a href="#tour-overview" title=""><?=TranslateController::t('Overview')?></a>
                            <a href="#optional-acitivites" title=""><?=TranslateController::t('Optional activities')?></a>
                            <a href="#accomodation" title=""><?=TranslateController::t('Accomodation')?></a>
                            <a href="#tour-necessary"><?=TranslateController::t('No Tip Necessary')?></a>
                        </div>
                    </div>
                    <div class="col-lg-9 tour-meals-cn">
                        <div class="tour-detail-text">
                            <ul class="tour-meals-gallery">
                                <?php
                                $urls = explode(';',$model->meals_img);
                                foreach ($urls as $item) {
                                    if(!empty($item)){
                                        ?>
                                            <li><?=Html::img('/'.$item)?></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                            <div>
                                <?=$model->meals?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Meals-->

            <!-- Necessary -->
            <section class="tour-necessary detail-cn" id="tour-necessary">
                <div class="row">
                    <div class="col-lg-3 detail-sidebar">
                        <div class="scroll-heading">
                            <h2><?=TranslateController::t('No Tip Necessary')?></h2>
                            <hr class="hr">
                            <a href="#tour-overview" title=""><?=TranslateController::t('Overview')?></a>
                            <a href="#optional-acitivites" title=""><?=TranslateController::t('Optional activities')?></a>
                            <a href="#accomodation" title=""><?=TranslateController::t('Accomodation')?></a>
                            <a href="#tour-meals"><?=TranslateController::t('Meals')?></a>
                        </div>
                    </div>
                    <div class="col-lg-9 tour-necessary-cn">
                        <div class="tour-detail-text">
                            <?=$model->no_tip_necessary?>
                        </div>

                    </div>
                </div>
            </section>
            <!-- End Necessary-->

            <section class="detail-footer tour-detail-footer detail-cn">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-9 detail-footer-cn text-right">
                        <p class="price-book">
                            <?=TranslateController::t('From')?>-<span>$<?=$model->prise*(1-$model->sale/100)?></span>/<?=$model->price_type?>
                            <a href="" title="" class="awe-btn awe-btn-1 awe-btn-lager">Book Now</a>
                        </p>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>