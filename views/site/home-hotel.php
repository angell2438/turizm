<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 17.05.2016
 * Time: 1:25
 */
use yii\helpers\Html;
?>
<!--Banner-->
<section class="banner">

    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->

    <div class="container">

        <!-- Banner Content -->
        <div class="banner-cn">

            <!-- Tabs Cat Form -->
            <ul class="tabs-cat text-center row">
                <li class="cate-item col-xs-2">
                    <a data-toggle="tab" href="#form-flight" title="">
                        <span>Перелет</span>
                        <?=Html::img("/images/icon-flight.png")?>
                    </a>
                </li>
                <li class="cate-item active col-xs-2">
                    <a data-toggle="tab" href="#form-hotel" title=""><span>Готель</span><?=Html::img("/images/icon-hotel.png")?></a>
                </li>
                <li class="cate-item col-xs-2">
                    <a data-toggle="tab" href="#form-car" title=""><span>Автобуси</span><?=Html::img("/images/icon-car.png")?></a>
                </li>
                <li class="cate-item col-xs-2">
                    <a data-toggle="tab" href="#form-package" title=""><span>Спец предложения</span><?=Html::img("/images/icon-tour.png")?></a>
                </li>
                <li class="cate-item col-xs-2">
                    <a data-toggle="tab" href="#form-cruise" title=""><span>круиз</span><?=Html::img("/images/icon-cruise.png")?></a>
                </li>
                <li class="cate-item col-xs-2">
                    <a data-toggle="tab" href="#form-tour" title=""><span>Тур</span><?=Html::img("/images/icon-vacation.png")?></a>
                </li>
            </ul>
            <!-- End Tabs Cat -->

            <!-- Tabs Content -->
            <div class="tab-content">

                <!-- Search Hotel -->
                <div class="form-cn form-hotel tab-pane active in" id="form-hotel">
                    <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                    <div class="form-search clearfix">
                        <div class="form-field field-destination">
                            <label for="destination"><span>Назначение:</span> Страна, Город, Аэропорт, область</label>
                            <input type="text" id="destination" class="field-input">
                        </div>
                        <div class="form-field field-date">
                            <input type="text" class="field-input calendar-input" placeholder="Дата начала">
                        </div>
                        <div class="form-field field-date">
                            <input type="text" class="field-input calendar-input" placeholder="Дата конца">
                        </div>
                        <div class="form-field field-select">
                            <div class="select">
                                <span>Количество</span>
                                <select>
                                    <option>Один</option>
                                    <option>Два</option>
                                    <option>Три</option>
                                    <option>Больше 3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-lager awe-search">Поиск</button>
                        </div>
                    </div>
                </div>
                <!-- End Search Hotel -->

                <!-- Search Car -->
                <div class="form-cn form-car tab-pane" id="form-car">
                    <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                    <div class="form-search clearfix">
                        <div class="form-field field-destination">
                            <label for="destination"><span>Назначение:</span> Страна, Город, Аэропорт, область</label>
                            <input type="text" id="destination" class="field-input">
                        </div>
                        <div class="form-field field-droping">
                            <input type="text" class="field-input" placeholder="Droping off">
                        </div>
                        <div class="form-field field-date">
                            <input type="text" class="field-input calendar-input" placeholder="Дата начала">
                        </div>
                        <div class="form-field field-date">
                            <input type="text" class="field-input calendar-input" placeholder="Дата конца">
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-lager awe-search">Поиск</button>
                        </div>
                    </div>
                </div>
                <!-- End Search Car -->

                <!-- Search Cruise-->
                <div class="form-cn form-cruise tab-pane" id="form-cruise">
                    <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                    <ul class="form-radio">
                        <li>
                            <div class="radio-checkbox">
                                <input type="radio" name="radio-2" id="radio-5" class="radio">
                                <label for="radio-5">Популярный кризы</label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-checkbox">
                                <input type="radio" name="radio-2" id="radio-6" class="radio">
                                <label for="radio-6">Круиз люкс</label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-checkbox">
                                <input type="radio" name="radio-2" id="radio-7" class="radio">
                                <label for="radio-7">Круиз по реке</label>
                            </div>
                        </li>
                    </ul>
                    <div class="form-search clearfix">
                        <div class="form-field field-destination">
                            <label for="destination2"><span>Назначение:</span> Страна...</label>
                            <input type="text" id="destination2" class="field-input">
                        </div>
                        <div class="form-field field-select field-lenght">
                            <div class="select">
                                <span>Длина круиза</span>
                                <select>
                                    <option>......</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-month">
                            <div class="select">
                                <span>Месяц</span>
                                <select>
                                    <option>Май</option>
                                    <option>Сентябрь</option>
                                    <option>Илюль</option>
                                    <option>Июнь</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-port">
                            <div class="select">
                                <span>Порт отправки</span>
                                <select>
                                    <option>...</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <!--
                                                        <div class="form-field field-select field-line">
                                                            <div class="select">
                                                                <span>Cruise Line</span>
                                                                <select>
                                                                    <option>Cruise Line</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                </select>
                                                            </div>
                                                        </div>
                        -->
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-medium awe-search">Поиск</button>
                        </div>
                    </div>
                </div>
                <!-- End Search Cruise -->

                <!-- Search Flight-->
                <div class="form-cn form-flight tab-pane" id="form-flight">
                    <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                    <div class="form-search clearfix">
                        <div class="form-field field-from">
                            <label for="flight-from"><span>Вилет из:</span> аеропорт</label>
                            <input type="text" name="flightfrom" id="flight-from" class="field-input">
                        </div>
                        <div class="form-field field-to">
                            <label for="flight-to"><span>В :</span> странна, аеропорт</label>
                            <input type="text" id="flight-to" class="field-input">
                        </div>
                        <div class="form-field field-date">
                            <input type="text" class="field-input calendar-input" placeholder="Departing">
                        </div>
                        <div class="form-field field-date">
                            <input type="text" class="field-input calendar-input" placeholder="Returning">
                        </div>
                        <div class="form-field field-select field-adult">
                            <div class="select">
                                <span>Взрослые</span>
                                <select>
                                    <option>один</option>
                                    <option>два</option>
                                    <option>три</option>
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-children">
                            <div class="select">
                                <span>Дети</span>
                                <select>
                                    <option>один</option>
                                    <option>два</option>
                                    <option>три</option>
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-medium awe-search">Поиск</button>
                        </div>
                    </div>
                </div>
                <!-- End Search Flight -->

                <!-- Search Package -->
                <div class="form-cn form-package tab-pane" id="form-package">
                    <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                    <ul class="form-radio">
                        <li>
                            <div class="radio-checkbox">
                                <input type="radio" name="radio-1" id="radio-1" class="radio">
                                <label for="radio-1">Полет+ Отель</label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-checkbox">
                                <input type="radio" name="radio-1" id="radio-2" class="radio">
                                <label for="radio-2">Полет+Отель+Машина</label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-checkbox">
                                <input type="radio" name="radio-1" id="radio-3" class="radio">
                                <label for="radio-3">Отель+Машина</label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-checkbox">
                                <input type="radio" name="radio-1" id="radio-4" class="radio">
                                <label for="radio-4">Полет+Отель</label>
                            </div>
                        </li>

                    </ul>
                    <div class="form-search clearfix">
                        <div class="form-field field-from">
                            <label for="filghtfrom"><span>Вылет из:</span> аэропорт...</label>
                            <input type="text" id="filghtfrom" class="field-input">
                        </div>
                        <div class="form-field field-to">
                            <label for="flightto"><span>В:</span> Странна, аэропорт</label>
                            <input type="text" id="flightto" class="field-input">
                        </div>
                        <div class="form-field field-date">
                            <input type="text" class="field-input calendar-input" placeholder="Departing">
                        </div>
                        <div class="form-field field-date">
                            <input type="text" class="field-input calendar-input" placeholder="Returning">
                        </div>
                        <div class="form-field field-select field-adults">
                            <div class="select">
                                <span>Взрослые</span>
                                <select>
                                    <option>один</option>
                                    <option>два</option>
                                    <option>три</option>
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-children">
                            <div class="select">
                                <span>Дети</span>
                                <select>
                                    <option>один</option>
                                    <option>два</option>
                                    <option>три</option>
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-medium awe-search">Поиск</button>
                        </div>
                    </div>
                </div>
                <!-- End Search Package -->

                <!-- Search Tour-->
                <div class="form-cn form-tour tab-pane" id="form-tour">
                    <!--                            <h2>Куда бы вы хотели пойти?</h2>-->
                    <div class="form-search clearfix">
                        <div class="form-field field-select field-region">
                            <div class="select">
                                <span>Регион: <small>африка..</small></span>
                                <select>
                                    <option>Африка</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-country">
                            <div class="select">
                                <span>Странна</span>
                                <select>
                                    <option>украина</option>
                                    <option>росия</option>
                                    <option>США</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-field field-select field-style">
                            <div class="select">
                                <span>Тип тура</span>
                                <select>

                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-submit">
                            <button type="submit" class="awe-btn awe-btn-medium awe-search">Поиск</button>
                        </div>
                    </div>
                </div>
                <!-- End Search Tour -->

            </div>
            <!-- End Tabs Content -->

        </div>
        <!-- End Banner Content -->

    </div>

</section>
<!--End Banner-->

<!-- Sales -->
<section class="sales">
    <!-- Title -->
    <div class="title-wrap">
        <div class="container">
            <div class="travel-title float-left">
                <h2>Горячая продажа Сегодня: <span>Париж, Амстердам, Санкт-Петербург и более</span></h2>
            </div>
            <a href="#" title="" class="awe-btn awe-btn-5 awe-btn-lager arrow-right text-uppercase float-right">ВСЕ ПРОДАЖИ</a>
        </div>
    </div>
    <!-- End Title -->

    <!-- Hot Sales Content -->
    <div class="container">
        <div class="sales-cn">
            <div class="row">

                <!-- HostSales Item -->
                <div class="col-xs-6 col-md-3">
                    <div class="sales-item">
                        <figure class="home-sales-img">
                            <a href="hotel-detail.html" title="">
                                <?=Html::img("/images/deal/img-1.jpg")?>
                            </a>
                            <figcaption>
                                Save <span>30</span>%
                            </figcaption>
                        </figure>
                        <div class="home-sales-text">
                            <div class="home-sales-name-places">
                                <div class="home-sales-name">
                                    <a href="hotel-detail.html" title="">Copley Square Hotel</a>
                                </div>
                                <div class="home-sales-places">
                                    <a href="" title="">Boston</a>,
                                    <a href="" title="">Massachusetts</a>
                                </div>
                            </div>
                            <hr class="hr">
                            <div class="price-box">
                                <span class="price old-price">From  <del>$269</del></span>
                                <span class="price special-price">$175<small>/night</small></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End HostSales Item -->

                <!-- HostSales Item -->
                <div class="col-xs-6 col-md-3">
                    <div class="sales-item">
                        <figure class="home-sales-img">
                            <a href="hotel-detail.html" title="">
                                <?=Html::img("/images/deal/img-2.jpg")?>
                            </a>
                            <figcaption>
                                Save <span>30</span>%
                            </figcaption>
                        </figure>

                        <div class="home-sales-text">
                            <div class="home-sales-name-places">
                                <div class="home-sales-name">
                                    <a href="hotel-detail.html" title="">Grand Hotel Bagni Nuovi</a>
                                </div>
                                <div class="home-sales-places">
                                    <a href="" title="">Boston</a>,
                                    <a href="" title="">Italy</a>
                                </div>
                            </div>
                            <hr class="hr">
                            <div class="price-box">
                                <span class="price old-price">From  <del>$632</del></span>
                                <span class="price special-price">$345<small>/night</small></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End HostSales Item -->

                <!-- HostSales Item -->
                <div class="col-md-6">
                    <div class="sales-item ">
                        <figure class="home-sales-img">
                            <a href="hotel-detail.html" title="">
                                <?=Html::img("/images/deal/img-3.jpg")?>
                            </a>
                            <figcaption>
                                Save <span>30</span>%
                            </figcaption>
                        </figure>
                        <div class="home-sales-text">
                            <div class="home-sales-name-places">
                                <div class="home-sales-name">
                                    <a href="hotel-detail.html" title="">The Standard, East Village</a>
                                </div>
                                <div class="home-sales-places">
                                    <a href="" title="">New York</a>,
                                    <a href="" title="">New York</a>
                                </div>
                            </div>
                            <hr class="hr">
                            <div class="price-box">
                                <span class="price old-price">From  <del>$582</del></span>
                                <span class="price special-price">$258<small>/night</small></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End HostSales Item -->

                <!-- HostSales Item -->
                <div class="col-md-6">
                    <div class="sales-item">
                        <figure class="home-sales-img">
                            <a href="hotel-detail.html" title="">
                                <?=Html::img("/images/deal/img-4.jpg")?>
                            </a>
                            <figcaption>
                                Save <span>30</span>%
                            </figcaption>
                        </figure>
                        <div class="home-sales-text">
                            <div class="home-sales-name-places">
                                <div class="home-sales-name">
                                    <a href="hotel-detail.html" title="">Ganges River Cruise</a>
                                </div>
                                <div class="home-sales-places">
                                    <a href="" title="">London</a>,
                                    <a href="" title="">United Kingdom</a>
                                </div>
                            </div>
                            <hr class="hr">
                            <div class="price-box">
                                <span class="price old-price">From  <del>$457</del></span>
                                <span class="price special-price">$258<small>/night</small></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End HostSales Item -->

                <!-- HostSales Item -->
                <div class="col-xs-6 col-md-3">
                    <div class="sales-item">
                        <figure class="home-sales-img">
                            <a href="hotel-detail.html" title="">
                                <?=Html::img("/images/deal/img-5.jpg")?>
                            </a>
                            <figcaption>
                                Save <span>30</span>%
                            </figcaption>
                        </figure>
                        <div class="home-sales-text">
                            <div class="home-sales-name-places">
                                <div class="home-sales-name">
                                    <a href="hotel-detail.html" title="">Town Hall Hotel</a>
                                </div>
                                <div class="home-sales-places">
                                    <a href="" title="">Boston</a>,
                                    <a href="" title="">Massachusetts</a>
                                </div>
                            </div>
                            <hr class="hr">
                            <div class="price-box">
                                <span class="price old-price">From  <del>$269</del></span>
                                <span class="price special-price">$175<small>/night</small></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End HostSales Item -->

                <!-- HostSales Item -->
                <div class="col-xs-6 col-md-3">
                    <div class="sales-item">
                        <figure class="home-sales-img">
                            <a href="hotel-detail.html" title="">
                                <?=Html::img("/images/deal/img-6.jpg")?>
                            </a>
                            <figcaption>
                                Save <span>30</span>%
                            </figcaption>
                        </figure>
                        <div class="home-sales-text">
                            <div class="home-sales-name-places">
                                <div class="home-sales-name">
                                    <a href="hotel-detail.html" title="">A Hidden NYC Mystery Hotel</a>
                                </div>
                                <div class="home-sales-places">
                                    <a href="" title="">Boston</a>,
                                    <a href="" title="">Italy</a>
                                </div>
                            </div>
                            <hr class="hr">
                            <div class="price-box">
                                <span class="price old-price">From  <del>$354</del></span>
                                <span class="price special-price">$255<small>/night</small></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End HostSales Item -->

            </div>
        </div>
    </div>
    <!-- End Hot Sales Content -->
</section>