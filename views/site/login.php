<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CityList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="login-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>

    <?= $form->field($model,'pass')->passwordInput()?>

    <?= $form->field($model,'rememberMe')->checkbox()?>

    <div class="form-group">
        <?= Html::submitButton('Sing in', ['class' => 'btn btn-success']) ?>
        <?=Html::a('Registration',['/site/reg'],['class' => 'btn btn-primary'])?>
    </div>

    <?php ActiveForm::end(); ?>

</div>