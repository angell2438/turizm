<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->
    <!-- Logo -->
    <div class="logo-banner text-center">
        <a href="" title="">
            <?=Html::img("/images/logo-banner.png")?>
        </a>
    </div>
    <!-- Logo -->
</section>
<!--End Banner-->

<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn bg-white clearfix">
            <!-- Breakcrumb -->
            <section class="breakcrumb-sc">
                <ul class="breadcrumb arrow">
                    <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                    <li>Contact us</li>
                </ul>
            </section>
            <!-- End Breakcrumb -->
            <section class="contact-page">
                <div class="contact-maps">
                    <div id="contact-maps" data-map-zoom="16" data-map-latlng="45.738028, 21.224535" data-map-content="Book Awesome"></div>
                </div>
                <div class="contact-cn">
                    <h2>Мы всегда на связи</h2>
                    <ul>
                        <li>
                            <?=Html::img("/images/icon-maker-contact.png")?>
                            25 California Avenue, Санта-Моника, Калифорния.
                        </li>
                        <li>
                            <?=Html::img("/images/icon-phone.png")?>
                            +1-888-8765-1234
                        </li>
                        <li>
                            <?=Html::img("/images/icon-email.png")?>
                            <a href="">contact@bookatrip.com</a>
                        </li>
                    </ul>
                    <div class="form-contact">
                        <form id="contact-forms" action="processContact.php" method="post">
                            <div class="form-field">
                                <label for="name">Имя <sup>*</sup></label>
                                <input type="text" name="name" id="name" class="field-input">
                            </div>
                            <div class="form-field">
                                <label for="email">Email <sup>*</sup></label>
                                <input type="text" name="email" id="email" class="field-input">
                            </div>
                            <div class="form-field form-field-area">
                                <label for="message">Сообщение <sup>*</sup></label>
                                <textarea name="message" id="message" cols="30" rows="10" class="field-input"></textarea>
                            </div>
                            <div class="form-field text-center">
                                <button type="submit" id="submit-contact" class="awe-btn awe-btn-2 arrow-right arrow-white awe-btn-lager">Отправить</button>
                            </div>
                            <div id="contact-content">
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- End Main -->
