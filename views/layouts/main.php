<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\widgets\LanguageWidget;
use app\commands\TranslateController;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?=Html::csrfMetaTags()?>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="preloader">
    <div class="tb-cell">
        <div id="page-loading">
            <div></div>
            <p>Loading</p>
        </div>
    </div>
</div>
<div class="wrap" >
    <?php require_once ('nav.php');?>
    <!--<div class="container">
        --><?/*= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) */?>
        <?= $content ?>
    <!--</div>-->
</div>
<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <!-- Logo -->
            <div class="col-md-4">
                <div class="logo-foter">
                    <?= Html::a(Html::img('/images/logo-header2.png'),['/site/index'])?>
                </div>
            </div>
            <!-- End Logo -->
            <!-- Navigation Footer -->
            <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="ul-ft">
                    <ul>
                        <li><?=Html::a(TranslateController::t('About company'),['/site/about'])?></li>
                        <li><?=Html::a(TranslateController::t('Blog'),['/site/blog'])?></li>
                        <li><?=Html::a(TranslateController::t('FQA'),['/site/fqa'])?></li>
                        <li><?=Html::a(TranslateController::t('Carrers'),['/site/careers'])?></li>
                    </ul>
                </div>
            </div>
            <!-- End Navigation Footer -->
            <!-- Navigation Footer -->
            <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="ul-ft">
                    <ul>
                        <li><?=Html::a(TranslateController::t('Contact us'),['/site/contact'])?></li>
                        <li><?=Html::a(TranslateController::t('Confidentiality'))?></li>
                        <li><?=Html::a(TranslateController::t('Life time'))?></li>
                        <li><?=Html::a(TranslateController::t('Security'))?></li>
                    </ul>
                </div>
            </div>
            <!-- End Navigation Footer -->
            <!-- Footer Currency, Language -->
            <div class="col-sm-6 col-md-4">
                <!-- Language -->
                <div class="currency-lang-bottom dropdown-cn float-left">
                    <div class="dropdown-head">
                        <span class="angle-down"><i class="fa fa-angle-down"></i></span>
                    </div>
                    <div class="dropdown-body">
                        <?= LanguageWidget::widget()?>
                    </div>
                </div>
                <!-- End Language -->
                <!-- Currency -->
                <div class="currency-lang-bottom dropdown-cn float-left">
                    <div class="dropdown-head">
                        <span class="angle-down"><i class="fa fa-angle-down"></i></span>
                    </div>
                    <div class="dropdown-body">
                        <ul>
                            <li class="current"><a href="#" title="">US</a></li>
                            <li><a href="#" title="">Rus</a></li>
                            <li><a href="#" title="">Uk</a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Currency -->
                <!--CopyRight-->
                <p class="copyright">
                    © 2009 – 2014 Bookyourtrip™ All rights reserved.
                </p>
                <!--CopyRight-->
            </div>
            <!-- End Footer Currency, Language -->
        </div>
    </div>
</footer>
<!-- End Footer -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
