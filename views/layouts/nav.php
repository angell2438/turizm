<?php
/**
 * Created by PhpStorm.
 * User: Masters2
 * Date: 16.05.2016
 * Time: 11:16
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\widgets\LanguageWidget;
use app\commands\TranslateController;
/*    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    */?>

<header id="header" class="header" style="min-height: 82px;">
        <?php
            NavBar::begin([
                'brandLabel' => '<div class="logo float-left">'. Html::img("/images/logo-header2.png").'</div>',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navigation nav-c nav-inner navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'menu-list text-uppercase navbar-nav navbar-right '],
                'items' => [
                    ['label' => TranslateController::t('Home'), 'url' => ['/site/index']],
                    ['label' => TranslateController::t('Pages'),
                        'items'=>[
                            ['label' => TranslateController::t('About'), 'url' => ['/site/about']],
                            Yii::$app->user->isGuest ? (
                            ['label' => TranslateController::t('Login'), 'url' => ['/site/login']]
                            ) : (
                                '<li>'
                                . Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                    TranslateController::t('Logout').' (' . Yii::$app->user->identity->username . ')',
                                    ['class' => 'btn btn-link']
                                )
                                . Html::endForm()
                                . '</li>'
                            ),
                            ['label' => 'Contacts', 'url' => ['/site/contacts']],
                            ['label' => TranslateController::t('Payment'),'url' =>['/site/payment']],
                            ['label' => TranslateController::t('Coming soon'),'url' =>['/site/comingsoon']],
                        ]
                    ],
                    ['label' => TranslateController::t('Hotel'),'url' =>['/site/home-hotel']],
                    ['label' => TranslateController::t('Flight'),'url' =>['/site/flight-list']],
                    ['label' => TranslateController::t('Bus'),'url' =>['/site/cart']],
                    ['label' => TranslateController::t('Packets'),'url' =>['/site/package-list']],
                    ['label' => TranslateController::t('Tours'),'url' =>['/site/tour-list']],




                ],
            ]);
            NavBar::end();
        ?>
</header>