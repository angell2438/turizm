<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;
use yii\helpers\Url;

AdminAsset::register($this);
$this->title = "Admin Panel"
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3" id="sidebar">
        <?php
                echo Nav::widget([
                    'options' => ['class' => 'nav nav-list bs-docs-sidenav nav-collapse'],
                    'items' => [
                        ['label' => 'Тури', 'url' => ['admin/tur/index']],
                        ['label' => 'Карїни', 'url' => ['/country/index']],
                        ['label' => 'Міста', 'url' => ['/city/index']],
                        ['label' => 'Готелі', 'url' => ['/hotel/index']],
                        ['label' => 'Тип номерів', 'url' => ['/room-type/index']],
                        ['label' => 'Дні туру', 'url' => ['/day-for-tur/index']],
                    ],
                ]);
                ?>
        </div>

        <!--/span-->
        <div class="span9" id="content">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>
    <hr>
    <footer>
        <p>&copy; Vincent Gabriel 2013</p>
    </footer>
</div>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>