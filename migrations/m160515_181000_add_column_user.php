<?php

use yii\db\Migration;

class m160515_181000_add_column_user extends Migration
{
    /*public function up()
    {

    }

    public function down()
    {
        echo "m160515_181000_add_column_user cannot be reverted.\n";

        return false;
    }*/

    public function safeUp()
    {
        $this->createTable('role',[
            'id' => 'pk',
            'name'=> 'varchar(55)',
            'description' => 'varchar(255)',
            'default' =>'int'
        ]);
        $this->addColumn('user','role_id','int');
        $this->addForeignKey('role_id','user','role_id','role','id','CASCADE','CASCADE');
        $this->batchInsert('role', ['name', 'description','default'], [
            ['user', 'user',1],
            ['admin', 'Admin',0],
        ]);

    }

    public function safeDown()
    {
        $this->dropForeignKey('role_id','user');
        $this->dropColumn('user','role_id');
        $this->dropTable('role');
    }
}
