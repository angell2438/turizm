<?php

use yii\db\Migration;

class m160513_074402_facilities extends Migration
{
    /*public function up()
    {

    }

    public function down()
    {
        echo "m160513_074402_facilities cannot be reverted.\n";

        return false;
    }*/


    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('facilities',[
            'id' =>'pk',
            'name'=>'varchar(55)',
            'img' => 'varchar(255)'
        ]);

        $this->batchInsert('facilities', ['name', 'img'], [
            ['Free breakfast', 'images/icon-service-1.png',],
            ['Spa service', 'images/icon-service-2.png',],
            ['Free parking', 'images/icon-service-3.png',],
            ['Sauna service', 'images/icon-service-4.png',],
            ['Restaurant', 'images/icon-service-5.png',],
            ['Casino', 'images/icon-service-6.png',],
            ['Swimming pool', 'images/icon-service-7.png',],
            ['Airport transfer', 'images/icon-service-8.png',],
            ['Free Wi-Fi in all rooms', 'images/icon-service-9.png',],
            ['Smoking area', 'images/icon-service-10.png',],
            ['Laundry service', 'images/icon-service-11.png',],
            ['Business center', 'images/icon-service-12.png',],
            ['Hair dryer', 'images/icon-service-13.png',],
            ['24-hour front desk', 'images/icon-service-14.png',],
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('facilities');
    }



}
