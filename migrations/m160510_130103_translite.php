<?php

use yii\db\Migration;

class m160510_130103_translite extends Migration
{
    public function up()
    {
        $this -> createTable('translate',[
            'id' => 'pk',
            'lang_id'  => 'int',
            'key'  => 'varchar(255)',
            'mess'  => 'varchar(255)',
        ]);
        $this->addForeignKey('lang_id','translate','lang_id', 'lang','id','CASCADE', 'CASCADE' );
    }

    public function down()
    {
        $this -> dropTable('translate');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
