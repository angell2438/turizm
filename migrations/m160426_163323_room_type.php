<?php

use yii\db\Migration;

class m160426_163323_room_type extends Migration
{
    public function up()
    {
        $this->createTable('room_type',[
            'id'            =>  'pk',
            'name'          =>  'varchar(255) NOT NULL',
            'description'   =>  'varchar(255)',
            'prise'         =>  'int NOT NULL',
            'count'         =>  'int',
            'free'          =>  'int',
            'hotel_id'      =>  'int',
            'number_seats'  =>  'int',
            'count_room'    =>  'int'
        ]);
    }

    public function down()
    {
        $this->dropTable('room_type');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
