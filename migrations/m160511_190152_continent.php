<?php

use yii\db\Migration;

class m160511_190152_continent extends Migration
{
    public function up()
    {
        $this->createTable('continent',[
            'id'=>'pk',
            'name'=>'varchar(255)'
        ]);
        $this->batchInsert('continent', ['name'], [
            ['Australia'],
            ['Europe'],
            ['Asia'],
            ['USA'],
            ['The remaining part of the world'],
        ]);
        $this->addColumn('tur','continent_id','int');
        $this->addForeignKey('continent_idtur','tur','continent_id', 'continent','id','CASCADE', 'CASCADE' );
    }

    public function down()
    {
       $this->dropTable('continent');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
