<?php

use yii\db\Migration;

class m160515_204556_add_column_day_ft extends Migration
{
    public function up()
    {
        $this -> addColumn('day_for_tur','number_day','int');
    }

    public function down()
    {
        $this->dropColumn('day_for_tur','number_day');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
