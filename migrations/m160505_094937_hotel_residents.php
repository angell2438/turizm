<?php

use yii\db\Migration;

class m160505_094937_hotel_residents extends Migration
{
    public function up()
    {
        $this->createTable('hotel_residents',[
            'id'            =>  'pk',
            'user_id'       =>  'int',
            'hotel_id'      =>  'int',
            'room_type_id'  =>  'int',
            'check_in'      =>  'int',
            'check_out'     =>  'int',
        ]);
    }

    public function down()
    {
        $this ->dropTable('hotel_residents');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
