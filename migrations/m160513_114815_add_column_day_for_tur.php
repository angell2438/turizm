<?php

use yii\db\Migration;

class m160513_114815_add_column_day_for_tur extends Migration
{
    public function up()
    {
        $this ->addColumn('day_for_tur','create_at','int');
        $this ->addColumn('day_for_tur','update_at','int');
    }

    public function down()
    {
        $this->dropColumn('day_for_tur','create_at');
        $this->dropColumn('day_for_tur','update_at');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
