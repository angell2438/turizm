<?php

use yii\db\Migration;

class m160426_163234_tur extends Migration
{
    public function up()
    {
        $this->createTable('tur',[
            'id'                    =>  'pk',
            'name'                  =>  'varchar(255) NOT NULL',
            'description'           =>  'text',
            'prise'                 =>  'int',
            'price_type'            =>  'varchar(55) NOT NULL',
            'type'                  =>  'varchar(50)',
            'sale'                  =>  'int',
            'long_time'             =>  'int',
            'visit'                 =>  'int',
            'country_id'            =>  'int NOT NULL ',
            'city_id'               =>  'int',
            'img'                   =>  'varchar(255)',
            'accommodation'         =>  'text',
            'optional_activities'   =>  'text',
            'meals'                 =>  'text',
            'meals_img'             =>  'text',
            'no_tip_necessary'      =>  'text'
        ]);
    }

    public function down()
    {
        $this->dropTable('tur');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
