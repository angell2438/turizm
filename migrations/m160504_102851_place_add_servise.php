<?php

use yii\db\Migration;

class m160504_102851_place_add_servise extends Migration
{
    public function up()
    {
        $this -> createTable('place_add_services',[
            'id'            =>  'pk',
            'place'         =>  'varchar(255)',
            'country_id'    =>  'int NOT NULL ',
            'city_id'       =>  'int',
        ]);
    }

    public function down()
    {
        $this->dropTable('place_add_services');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
