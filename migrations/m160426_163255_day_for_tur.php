<?php

use yii\db\Migration;

class m160426_163255_day_for_tur extends Migration
{
    public function up()
    {
        $this->createTable('day_for_tur',[
            'id'            => 'pk',
            'name'         => 'varchar(255) NOT NULL',
            'description'      => 'varchar(255)',
            'country_id'        => 'int NOT NULL ',
            'city_id'        => 'int',
            'tur_id'        => 'int',
        ]);
    }

    public function down()
    {
        $this->dropTable('day_for_tur');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
