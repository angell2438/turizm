<?php

use yii\db\Migration;

class m160512_170057_reviews_tour extends Migration
{
    public function up()
    {
        $this->createTable('reviews_tour',[
            'id' => 'pk',
            'tur_id' => 'int',
            'user_id'=>'int',
            'name' =>'string',
            'transport_type'=>'string',
            'create_at'=>'int',
            'update_at'=>'int',
            'description'=>'text',
            'type_review'=>'int'
        ]);
        $this->addForeignKey('tur_id_reviews_tour','reviews_tour','tur_id', 'tur','id','CASCADE', 'CASCADE' );
        $this->addForeignKey('user_id_reviews_tour','reviews_tour','user_id', 'user','id','CASCADE', 'CASCADE' );
    }

    public function down()
    {
       $this->dropTable('reviews_tour');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
