<?php

use yii\db\Migration;

class m160426_163311_hotel extends Migration
{
    public function up()
    {
        $this->createTable('hotel',[
            'id'                =>  'pk',
            'name'              =>  'varchar(255) NOT NULL',
            'description'       =>  'varchar(255)',
            'country_id'        =>  'int NOT NULL ',
            'city_id'           =>  'int',
            'star'              =>  'int',
            'facilities'        =>  'varchar(255)',
            'language_id'       =>  'varchar(255)',
            'details_policies'  =>  'text',
            'area'              =>  'text',
            'area_coordinates'  =>  'text',
            'discount'          =>  'int'
        ]);
    }

    public function down()
    {
        $this->dropTable('hotel');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
