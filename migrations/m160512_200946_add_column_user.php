<?php

use yii\db\Migration;

class m160512_200946_add_column_user extends Migration
{
    public function up()
    {
        $this->addColumn('user','username','varchar(55)');
        $this->addColumn('user','status','int');
        $this->addColumn('user','auth_key','varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('user','username');
        $this->dropColumn('user','status');
        $this->dropColumn('user','auth_key');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
