<?php

use yii\db\Migration;

class m160426_163356_city extends Migration
{
    public function up()
    {
        $this->createTable('crm_city_list', [
            'id'            => 'pk',
            'country_id'    => 'int default 0',
            'title'         => 'varchar(255) NOT NULL',
            'state'         => 'varchar(255)',
            'region'        => 'varchar(255)',
            'biggest_city'  => 'int NOT NULL default 0'
        ]);

        $this->createTable('crm_country_list', [
            'id'            => 'pk',
            'title'         => 'varchar(255) NOT NULL',
            'iso_code'      => 'varchar(255)',
            'status'        => 'int NOT NULL default 1'
        ]);

    }

    public function down()
    {
        $this->dropTable('crm_city_list');
        $this -> dropTable('crm_country_list');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
