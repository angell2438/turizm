<?php

use yii\db\Migration;

class m160504_102141_additional_services extends Migration
{
    public function up()
    {
        $this->createTable('add_services',[
            'id'            =>  'pk',
            'name'          =>  'varchar(255) NOT NULL',
            'description'   =>  'varchar(255)',
            'price'         =>  'int',
            'price_type'    =>  'varchar(55)',
            'tur_id'        =>  'int',
            'place_id'      =>  'int'
        ]);
    }

    public function down()
    {
        $this -> dropTable('add_services');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
