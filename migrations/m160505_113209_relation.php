<?php

use yii\db\Migration;

class m160505_113209_relation extends Migration
{
    public function up()
    {
        //звязки для таблиці tur
        $this->addForeignKey('country_idtur','tur','country_id', 'crm_country_list','id','CASCADE', 'CASCADE' );
        $this->addForeignKey('city_idtur','tur','city_id', 'crm_city_list','id','CASCADE', 'CASCADE' );

        //звязки для таблиці day_for_tur
        $this->addForeignKey('country_idday_for_tur','day_for_tur','country_id', 'crm_country_list','id','CASCADE', 'CASCADE' );
        $this->addForeignKey('city_idday_for_tur','day_for_tur','city_id', 'crm_city_list','id','CASCADE', 'CASCADE' );
        $this->addForeignKey('tur_idday_for_tur','day_for_tur','tur_id', 'tur','id','CASCADE', 'CASCADE' );

        //звязки для таблиці hotel
        $this->addForeignKey('country_idhotel','hotel','country_id', 'crm_country_list','id','CASCADE', 'CASCADE' );
        $this->addForeignKey('city_idhotel','hotel','city_id', 'crm_city_list','id','CASCADE', 'CASCADE' );

        //звязки для таблиці crm_city_list
        $this->addForeignKey('country_idcrm_city_list','crm_city_list','country_id', 'crm_country_list','id','CASCADE', 'CASCADE' );

        //звязки для таблиці room_type
        $this->addForeignKey('hotel_idroom_type','room_type','hotel_id', 'hotel','id','CASCADE', 'CASCADE' );

        // add_services
        $this->addForeignKey('tur_idadd_services','add_services','tur_id', 'tur','id','CASCADE', 'CASCADE' );
        $this->addForeignKey('place_idadd_services','add_services','place_id', 'place_add_services','id','CASCADE', 'CASCADE' );

        //place_add_services
        $this->addForeignKey('country_idplace_add_services','place_add_services','country_id', 'crm_country_list','id','CASCADE', 'CASCADE' );
        $this->addForeignKey('city_idplace_add_services','place_add_services','city_id', 'crm_city_list','id','CASCADE', 'CASCADE' );

        //hotel_residents
        $this->addForeignKey('user_idhotel_residents','hotel_residents','user_id', 'user','id','CASCADE', 'CASCADE' );
        $this->addForeignKey('hotel_idhotel_residents','hotel_residents','hotel_id', 'hotel','id','CASCADE', 'CASCADE' );
        $this->addForeignKey('room_type_idhotel_residents','hotel_residents','room_type_id', 'room_type','id','CASCADE', 'CASCADE' );

        //reviews
        $this->addForeignKey('user_idreviews','reviews','user_id', 'user','id','CASCADE', 'CASCADE' );
        $this->addForeignKey('hotel_idreviews','reviews','hotel_id', 'hotel','id','CASCADE', 'CASCADE' );
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
