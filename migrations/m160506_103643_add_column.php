<?php

use yii\db\Migration;

class m160506_103643_add_column extends Migration
{
    public function up()
    {
        $this->addColumn('room_type','img','text');
        $this->addColumn('tur','date_start','int');
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
