<?php

use yii\db\Migration;

class m160505_111135_user extends Migration
{
    public function up()
    {
        $this -> createTable('user',[
            'id'            =>  'pk',
            'name'          =>  'varchar(55) NOT NULL',
            'last_name'     =>  'varchar(55) NOT NULL',
            'surname'       =>  'varchar(55) NOT NULL',
            'email'         =>  'varchar(255) NOT NULL',
            'pass'          =>  'varchar(255) NOT NULL',
            'avatar'        =>  'varchar(255)',
            'birthday'      =>  'int',
            'create_at'     =>  'int',
            'update_at'     =>  'int',
        ]);
    }

    public function down()
    {
        $this ->dropTable('user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
