<?php

use yii\db\Migration;

class m160505_105942_criterion extends Migration
{
    public function up()
    {
        $this ->createTable('criterion',[
            'id'            =>  'pk',
            'name'          =>  'varchar(55)',
            'description'   =>  'varchar(255)'
        ]);
    }

    public function down()
    {
        $this -> dropTable('criterion');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
