<?php

use yii\db\Migration;

class m160516_080024_add_column_tour extends Migration
{
    public function up()
    {
        $this->addColumn('tur','start','int');
        $this->addColumn('tur','end','int');
    }

    public function down()
    {
        $this -> dropColumn('tur','start');
        $this -> dropColumn('tur','end');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
