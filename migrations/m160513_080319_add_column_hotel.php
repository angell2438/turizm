<?php

use yii\db\Migration;

class m160513_080319_add_column_hotel extends Migration
{
    public function up()
    {
        $this ->addColumn('hotel','img','varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('hotel','img');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
