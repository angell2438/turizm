<?php

use yii\db\Migration;

class m160505_103958_reviews extends Migration
{
    public function up()
    {
        $this   ->createTable('reviews',[
            'id'            =>  'pk',
            'user_id'       =>  'int',
            'description'   =>  'text',
            'hotel_id'      =>  'int',
            'plus'          =>  'int',//id критерію
            'minus'         =>  'int'//id критерію
        ]);
    }

    public function down()
    {
        $this -> dropTable('reviews');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
