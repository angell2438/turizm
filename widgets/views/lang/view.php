<?php
use yii\helpers\Html;
?>
<div id="lang" class="dropdown-body">
    <ul id="langs">
        <li class="current"><a href="#" title=""><?= $current->name;?></a></li>
        <?php foreach ($langs as $lang):?>
            <li class="item-lang">
                <?= Html::a($lang->name, '/'.$lang->url.Yii::$app->getRequest()->getLangUrl()) ?>
            </li>
        <?php endforeach;?>
    </ul>
</div>

<!--<div class="dropdown-body">
    <ul>

        <li><a href="#" title="">English</a></li>
        <li><a href="#" title="">Russian</a></li>
        <li><a href="#" title="">Deutsch</a></li>
        <li><a href="#" title="">Francais</a></li>
        <li><a href="#" title="">Italiano</a></li>
    </ul>
</div>-->