function ajaxFindCity(selector,id){
    $.ajax({
        url:"/admin/tur/find-city",
        method:"POST",
        data:{
            id:id
        },
        success: function(data){
            data = JSON.parse(data);
            if(data.length!=0){
                $('#'+selector+'-city_id').html('');
            }
            else {
                $('#'+selector+'-city_id').html('<option value=""></option>');
            }

            data.forEach(function(entry) {
                $('#'+selector+'-city_id').append('<option value="'+entry.id+'">'+entry.title+'</option>');
            });
        }
    })
}

function parseGetParams() {
    var $_GET = {};
    var __GET = window.location.search.substring(1).split("&");
    for(var i=0; i<__GET.length; i++) {
        var getVar = __GET[i].split("=");
        $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1];
    }
    return $_GET;
}

$(document).ready(function(){
    $('#tur-country_id, #hotel-country_id, #dayfortur-country_id').change(function(){
        var selector = $(this);
        selector = selector[0]['id'];
        selector = selector.split('-')[0];
        var id = $(this).val();
        //
        ajaxFindCity(selector,id);
    });
    $('.close').on('click',function(){
        if(confirm("Видалити зображення?")){
            var paren= $(this).parent();
            var id = parseGetParams()['id'];
            var url = $(this).data('url');
            var from = $(this).data('from');
            var controler = $(this).data('model');
            $.ajax({
                url:"/admin/"+controler+"/delete-img",
                method:"POST",
                data:{
                    id:id,
                    url:url,
                    from:from,
                },
                success: function(data){
                    if(data==1){
                        paren.remove();
                    }
                }
            })
        }

    });


});