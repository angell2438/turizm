/**
 * Created by Ivan on 12.05.2016.
 */
function filter(form){
    $.ajax({
        url:'/site/tur-filter',
        type:"POST",
        data:form.serialize(),
        success: function(data){
            $('.cruise-list-cn.tour-list-cn').html(data);
            //data = JSON.parse(data);console.log(data);

        }
    });
}
$(document).ready(function(){
    $('.tur-search,.radio-checkbox input').on('click',function(){
        var form = $(this).parents().find('form.search-form_tour');
        filter(form)
    });
    $('.sort-select').on('change',function(){
        var form = $(this).parents().find('form.search-form_tour');
        filter(form);
    });

    $('.test input').click(function(){
        var form = $(this).parents().find('form.search-form_tour');
        filter(form)
    });
});
