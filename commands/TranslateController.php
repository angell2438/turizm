<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\Translate;
use app\models\Lang;
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TranslateController extends Controller
{
    public static function t($key)
    {
        $id = Lang::find()->select('id')->where('local=:loc',[':loc'=>\Yii::$app->language])->one();
        $mess = Translate::find()
            ->where(['key'=>$key])
            ->andWhere(['lang_id'=>$id->id])
            ->one();
        if(!empty($mess)){
            if(!empty($mess->mess)){
                return $mess->mess;
            }
            else{
                return $key;
            }
        }
        else{
            $model = new Translate();
            $model->lang_id = $id->id;
            $model->key = $key;
            $model->mess = '';
            $model->save();
            return $key;
        }
    }
}